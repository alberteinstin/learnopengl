#version 330
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;

out vec2 TexCoords;
out vec3 FragPos;
out vec3 Normal;

uniform mat4 Model;
uniform mat4 View;
uniform mat4 Project;

void main()
{
   gl_Position = Project * View * Model * vec4(aPos, 1.0);
   FragPos = vec3( Model * vec4(aPos, 1.0));
    TexCoords = aTexCoords;
//       Normal =  mat3(transpose(inverse(Model))) * aNormal;
    Normal = aNormal;
}
