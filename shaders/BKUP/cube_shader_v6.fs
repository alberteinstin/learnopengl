#version 330 core
out vec4 FragColor;

struct Material {
    sampler2D diffuse;
     sampler2D specular;    
    float shininess;
}; 
// positional light source (point lights)
struct PosLight {
    vec3 pos;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
};

// directional light source (like sky!)
struct DirLight {
    vec3 dir;     // direction (caster!)
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};


in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;

uniform vec3 viewPos;   // coming from camera position

uniform Material material;
uniform Light light;

uniform bool spec_on;
uniform bool diff_on;
uniform bool amb_on;

void main()
{
    float light_distance = length (light.pos - FragPos);
    float light_attenuation = 1.0 / (light.constant + light.linear * light_distance + light.quadratic * light_distance * light_distance);
    
  vec3 ambient = light.ambient * texture(material.diffuse,TexCoords).rgb;
  // diffuse
    vec3 norm = normalize(Normal);
    
    vec3 light_dir = normalize(-light.dir);
    if (light.pos != vec3(0.f))
       light_dir = normalize(light.pos - FragPos);

   float diff_factor =  max(dot(norm,light_dir),0.f);
   vec3 diffuse = light.diffuse * diff_factor * texture(material.diffuse,TexCoords).rgb;

// specular
     vec3 view_dir = normalize(viewPos - FragPos);
    vec3 reflect_dir = reflect(-light_dir,norm);
   
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0), material.shininess);
    vec3 specular    = light.specular * spec * texture(material.specular,TexCoords).rgb;

//    vec3 result = ambient + diffuse  + specular;

      ambient *= light_attenuation;
     specular *= light_attenuation;
     diffuse *= light_attenuation;

    vec3 result =  vec3(0.f);
  
    if (spec_on)  result += specular;
     if (amb_on)  result += ambient;
     if (diff_on) result += diffuse;
      
    FragColor = vec4(result, 1.0);
}