#version 330 core
out vec4 FragColor;

in vec3 Normal;
in vec3 FragPos;

uniform vec3 objectColor;
uniform vec3 lightColor;
uniform vec3 lightPos;
uniform vec3 viewPos;


void main()
{
    float ambient_strength = 0.1;
    float specular_strength = 0.5;
    float shininess = 128.0;
    
    vec3 ambient = ambient_strength * lightColor;
    vec3 norm = normalize(Normal);
    vec3 light_dir = normalize(lightPos - FragPos);
    vec3 view_dir = normalize (viewPos - FragPos);
    vec3 reflect_dir = reflect(-light_dir,norm);

    float spec = pow(max(dot(reflect_dir,light_dir), 0.0), shininess);
    vec3 specular = specular_strength * spec * lightColor;
    
    float diffuse_fctr = max(dot(light_dir,norm), 0.0);
    vec3 diffuse = diffuse_fctr * lightColor;
    
    FragColor = vec4((ambient + diffuse + specular) * objectColor, 1.0);
}