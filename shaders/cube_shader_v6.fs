#version 330 core
out vec4 FragColor;

struct Material {
    sampler2D diffuse;
     sampler2D specular;    
    float shininess;
};


// positional light source (point lights)
struct Light {
  vec3 ambient;
  vec3 diffuse;
  vec3 specular;
};

// positional light source (point lights)
struct PosLight {
  Light light;
  
  vec3  pos;
  float constant;
  float linear;
  float quadratic;
};

struct FlashLight {
  PosLight light;
  vec3 dir;
  float cos_cutoff;    // cos(radian(cone_angle));
  float cos_outer_cutoff;    // cos(radian(cone_angle));
};

#define NBR_POINT_LIGHTS 4
uniform PosLight point_light[NBR_POINT_LIGHTS];
// directional light source (like sky!)
struct DirLight {
  Light light;
  vec3 dir;     // direction (caster!)
 };


in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;

uniform vec3 viewPos;   // coming from camera position
uniform int nbr;

uniform Material material;
uniform PosLight pos_light;
uniform DirLight dir_light;
uniform FlashLight spot_light;

uniform bool spec_on;
uniform bool diff_on;
uniform bool amb_on;



vec3 throwLight (Light light, vec3 Dir, vec3 Norm, vec2 Tex_coord, Material material, float dim)
{
  vec3 light_dir = normalize(Dir);
    vec3 norm = normalize(Norm);
    vec3 reflect_dir = reflect(-light_dir,norm);
         
    vec3 ambient = light.ambient * texture(material.diffuse,Tex_coord).rgb;
// diffuse
    float diff_factor =  max(dot(norm,light_dir),0.f);
    vec3 diffuse = light.diffuse * diff_factor * texture(material.diffuse,Tex_coord).rgb;
// specular
     vec3 view_dir = normalize(viewPos - FragPos);
     
     float spec = pow(max(dot(view_dir, reflect_dir), 0.0), material.shininess);
     vec3 specular    = light.specular * spec * texture(material.specular,Tex_coord).rgb;
     
     ambient *= dim;
     specular *= dim;
     diffuse *= dim;
     
     vec3 result =  vec3(0.f);
     
     if (spec_on) result += specular;
     if (amb_on)  result += ambient;
     if (diff_on) result += diffuse;
     
     return result;
}

vec3 pointLight (PosLight MyLight, vec3 FragPos, vec3 Norm, vec2 Tex_coord, Material material)
{
  float distance = length (MyLight.pos - FragPos);
  float attenuation = 1.0 / (MyLight.constant + MyLight.linear * distance + MyLight.quadratic * distance * distance);
  vec3 light_dir =  MyLight.pos - FragPos;  // point towards light

  return throwLight(MyLight.light, light_dir, Norm, Tex_coord, material, attenuation);
}


vec3 flashLight (FlashLight MyLight, vec3 FragPos, vec3 Norm, vec2 Tex_coord, Material material)
{
  // get contribution of isotropic light source
  vec3 res =  pointLight (MyLight.light, FragPos, Norm, Tex_coord, material);

  // now limit it to a directional cone
  vec3 light_dir = normalize(MyLight.light.pos - FragPos);
  float cone = abs(dot(normalize(MyLight.dir),-light_dir));
  float delta = MyLight.cos_cutoff - MyLight.cos_outer_cutoff;      // these are cosine of cone angles
  float intensity = clamp ((cone - MyLight.cos_outer_cutoff)/delta,0.f, 1.f);
  
 res *= intensity;

  return res;
}

vec3 directionalLight (DirLight MyLight, vec3 FragPos, vec3 Norm, vec2 Tex_coord, Material material)
{
  return throwLight(MyLight.light, -MyLight.dir, Norm, Tex_coord, material, 1.0f);
}

void main()
{
  vec3 result = vec3(0.f);
     
  for (int i = 0; i < nbr; i++) {
    //      if (point_light[i].pos != vec3(0.f))
    result +=  pointLight(point_light[i], FragPos, Normal, TexCoords, material);
  }
  
   result +=  directionalLight(dir_light, FragPos, Normal, TexCoords, material);

  FlashLight s_light = spot_light;
  s_light.light.light.ambient  =  vec3(0.f);  
  s_light.light.light.diffuse  =  vec3(1.f);  
  s_light.light.light.specular  =  vec3(1.f);  
  s_light.light.constant  =  point_light[0].constant;
  s_light.light.linear  =  point_light[0].linear;
  s_light.light.quadratic  =  point_light[0].quadratic;
    
  result +=  flashLight(s_light, FragPos, Normal, TexCoords, material);
  
  
  FragColor = vec4(result, 1.0);
}
