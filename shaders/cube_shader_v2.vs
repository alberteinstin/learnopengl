#version 330
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTex;

out vec3 Normal;
out vec3 FragPos;
out vec2 TexCoord;

uniform mat4 Model;
uniform mat4 View;
uniform mat4 Project;

void main()
{
   gl_Position = Project * View * Model * vec4(aPos, 1.0);
 //  Normal = aNormal;
   Normal =  mat3(transpose(inverse(Model))) * aNormal;  // compensates for non-uniform scaling (expensive!)
   TexCoord = aTex;
   FragPos  =  vec3(Model * vec4(aPos, 1.0));
}
