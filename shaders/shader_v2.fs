#version 330 core
out vec4 FragColor;

in vec2 TexCoord;

uniform sampler2D Texture_0;
uniform sampler2D Texture_1;

void main()
{
    FragColor = mix(texture(Texture_0,TexCoord),texture(Texture_1,TexCoord),0.2f);
}