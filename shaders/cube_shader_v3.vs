#version 330
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;

out vec3 Normal;
out vec3 FragPos;

uniform mat3 NormalModel;   // precompute 
uniform mat4 Model;
uniform mat4 View;
uniform mat4 Project;

void main()
{
 //   Normal = aNormal;
   Normal = mat3(transpose(inverse(Model))) *  aNormal; // deal with non-uniform scaling
//   Normal = NormalModel *  aNormal; 
   FragPos = vec3(Model * vec4(aPos, 1.0));  // in world coords (to calculate dot product for diffuse
  gl_Position = Project * View * Model * vec4(aPos, 1.0);
}
