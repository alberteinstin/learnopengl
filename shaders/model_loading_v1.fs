#version 330 core

struct Material {
    sampler2D diffuse0;
    sampler2D specular0;
    sampler2D height0;  
    float shininess;
}; 

struct Light {
    vec3 pos;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};


out vec4 FragColor;

in vec2  TexCoords;
in vec3  FragPos;
in vec3  Normal;

uniform Material material;
uniform Light light;

uniform bool spec_on;
uniform bool diff_on;
uniform bool amb_on;

//uniform sampler2D texture_diffuse0;
//uniform sampler2D texture_diffuse0;

//uniform vec3 lightPos;
uniform vec3 viewPos;

void main ()
{
	vec3 light_ambient = light.ambient;
//        light_ambient  = vec3(0.1f);

  vec3 ambient = light_ambient * texture(material.diffuse0,TexCoords).rgb;
  // diffuse
    vec3 norm = normalize(Normal);
    vec3 light_dir = normalize(light.pos - FragPos);

   float diff_factor =  max(dot(norm,light_dir),0.f);
   vec3 light_diffuse = light.diffuse;
//   light_diffuse =  vec3(1.f);
   vec3 diffuse =  light_diffuse * diff_factor *  texture(material.diffuse0,TexCoords).rgb;

// specular
     vec3 view_dir = normalize(viewPos - FragPos);
    vec3 reflect_dir = reflect(-light_dir,norm);
   
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0), material.shininess);
    vec3  light_specular = light.specular;
//    light_specular = vec3(0.3f);
    vec3 specular    = light_specular * spec * texture(material.specular0,TexCoords).rgb;

//    vec3 result = ambient + diffuse  + specular;

    vec3 result =  vec3(0.f);
  
    if (spec_on)  result +=  specular; // light.specular; 
     if (amb_on)  result += ambient;
     if (diff_on) result += diffuse;
      
    FragColor = vec4(result, 1.0);
//    FragColor = texture(material.diffuse0,TexCoords);
}