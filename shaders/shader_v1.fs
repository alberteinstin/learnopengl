#version 330 core
out vec4 FragColor;

in vec3 ourColor;
in vec2 texCoord;

uniform sampler2D Texture_0;
uniform sampler2D Texture_1;

void main()
{
  // FragColor = texture(Texture_1,texCoord)* vec4(ourColor, 1.0); 
  FragColor = mix(texture(Texture_0,texCoord),texture(Texture_1,texCoord),0.2f);
  // FragColor = mix(texture(Texture_0,texCoord),texture(Texture_1,vec2(1.0-texCoord.x,texCoord.y)),0.2f);
}