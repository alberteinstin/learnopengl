#version 330 core
out vec4 FragColor;

struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;    
    float shininess;
}; 

struct Light {
    vec3 pos;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

in vec3 FragPos;
in vec3 Normal;

uniform vec3 viewPos;

uniform Material material;
uniform Light    light;

uniform bool spec_on;
uniform bool diff_on;
uniform bool amb_on;

void main()
{
    
    vec3 norm = normalize(Normal);
    vec3 light_dir = normalize(light.pos - FragPos);
    vec3 view_dir  = normalize(viewPos - FragPos);
    
    vec3 reflect_dir = reflect(-light_dir,norm);

    vec3 ambient = material.ambient * light.ambient;
    
    float diff_factor =  max(dot(norm,light_dir),0.f);
    vec3 diffuse = light.diffuse * diff_factor * material.diffuse;

    float spec_factor = max(dot(view_dir, reflect_dir), 0.0);
    float spec = pow(spec_factor, material.shininess);
    vec3 specular    = light.specular * (spec * material.specular);

 //   vec3 result = ambient + diffuse + specular;

    vec3 result =  vec3(0.f);
  
    if (spec_on)   result += specular;
 //   if (spec_on)   result += spec * vec3(1.0);
     if (amb_on)   result += ambient;
     if (diff_on)  result += diffuse;
 
    FragColor = vec4(result, 1.0);
}