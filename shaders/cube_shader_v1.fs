#version 330 core
out vec4 FragColor;

// in vec3 Normal;

uniform vec3 objectColor;
uniform vec3 lightColor;

void main()
{
    float ambient_strength = 1.0;
    FragColor = vec4(ambient_strength * lightColor * objectColor, 1.0);
}