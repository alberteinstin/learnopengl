#version 330 core
out vec4 FragColor;

in vec3 Normal;
in vec3 FragPos;

uniform vec3 lightPos;
uniform vec3 viewPos;

uniform vec3 objectColor;
uniform vec3 lightColor;

void main()
{
    float ambient_strength = 0.2;
    float specular_strength = 0.5;

    vec3 norm = normalize(Normal);
    vec3 light_dir = normalize(lightPos - FragPos);
    vec3 view_dir = normalize(viewPos - FragPos);
    vec3 reflect_dir = reflect(-light_dir,norm);
    
    vec3 diffuse = max(dot(norm,light_dir),0.f) * lightColor;
    vec3 specular    = specular_strength * pow(max(dot(norm, view_dir),0.f),32) * lightColor;
   
    vec3 ambient = ambient_strength * lightColor;
    FragColor = vec4( (ambient + diffuse + specular) * objectColor, 1.0);
}