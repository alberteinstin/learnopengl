//
 //  OpenGL
//
//  Created by Albert Einstin on 11/18/19.
//  Copyright © 2019 Albert Einstin. All rights reserved.
//

#ifndef CAMERA_H
#define CAMERA_H

#include <array>
#include <glad/glad.h> // include glad to get all the required OpenGL headers
  
//#include <string>
 #include "util.h"  // clamp

  

class Camera
{
  typedef Camera SELF;
    constexpr static glm::vec3 c_up {0.0f, 1.0f, 0.0f};            // straight up (along y-axis)
    constexpr static glm::vec3 c_front {0.0f, 0.0f, -1.0f};        // towards neg z-axis
    constexpr static glm::vec3 c_world_center {0.0f, 0.0f, 0.0f};
    
private:
    // camera ID
  int id_;
    glm::vec3 pos_,  up_, point_front_;  // <- where is pointing
    glm::vec3  local_up_;          // up dir in local camera coords
  float     pitch_, yaw_, fov_; // in deg

public:

    // constructor reads and builds the camera
    Camera(const glm::vec3& Pos, const glm::vec3& Target = glm::vec3(0.0f, 0.0f, 0.0f), float Pitch = 0.f, float Yaw   = - 90.f, float Fov = 45.f):
    id_{-1}, pos_{Pos},  local_up_{glm::vec3(0.0f, 1.0f, 0.0f)},  // it's fixed for now (roll inhibited)
    point_front_{glm::vec3(0.0f, 0.0f, -1.0f)},               // where is looking at (unit vec, towards neg-z axis)
    pitch_{Pitch}, yaw_{Yaw}, fov_{Fov}  {
        _update_up_(Target - pos_);
    }

  auto ID () const { return id_; }
  
    glm::vec3 Pos() const { return pos_; }
    auto& Pos() { return pos_; }   // l-val
    glm::vec3 Up() const { return up_; }
    glm::vec3 Front() const { return point_front_; }
    
    auto Pitch () const { return pitch_; }
    auto Yaw () const  { return yaw_; }
  float Fov () const { return fov_; } 
  float FovInRad () const { return glm::radians(fov_); } // in radians
  float Zoom () const { return fov_; }      // alias to Fov()
// <- useful to display (pitch, yaw, fov)
   auto PitchYawFov () const { std::array<float,3> pyf {pitch_, yaw_, fov_}; return pyf; }

  // view               in wold space            (position,    target,  up)
    glm::mat4 lookAt() const { return glm::lookAt(pos_, pos_ + point_front_,  up_); }
  
    // move sideways (Speed > 0 to the right, else to the left)
  SELF& _strafe(float Speed) {
      auto sideway_stp = glm::normalize(glm::cross(up_,point_front_)) ; pos_ += Speed * sideway_stp;
      return *this; }
  
  SELF& _zoom(float Speed) { pos_ += Speed * point_front_; return *this; }
    
  SELF& _updateYaw(float Off, bool doUpdate = true) {
      yaw_ += Off; return doUpdate ? _update_direction_() : *this; }
  SELF& _updatePitch(float Off, bool doUpdate = true, float Low = -89.f, float Hi = 89.f) {
      pitch_ = clamp( pitch_+= Off, Low, Hi); return doUpdate ? _update_direction_() : *this; }
  // update zoom (can be used by client to derive perspective matrix)
  SELF& _updateFov(float Off, float Low = 1.f, float Hi = 45.f) {
      fov_ = clamp( fov_ += Off, Low, Hi); return *this; }

    SELF& _reset(const glm::vec3& Pos, const glm::vec3& Target = glm::vec3(0.0f, 0.0f, 0.0f), float Pitch = 0.f, float Yaw   = - 90.f, float Fov = 45.f) {
        pos_ = Pos; point_front_ = glm::vec3(0.0f, 0.0f, -1.0f); pitch_ = Pitch; yaw_ = Yaw;  fov_ = Fov;
        return _update_up_(Target - pos_);
    }

private:
    // -> 'pos_' to target
    SELF& _update_up_(const glm::vec3& Direction) {
        point_front_ = glm::normalize(Direction);                                    // where is looking at (unit vec)
        auto direction_to_right = glm::normalize(glm::cross(point_front_, local_up_));   // not perpendicular
        up_ = glm::normalize(glm::cross(direction_to_right, point_front_));              // now `up_' is perpendicular to 'point_front_'
        return *this;
    }
  SELF& _update_direction_() {
      auto cos_pitch =  cos(glm::radians(pitch_));
      auto direction = glm::vec3{cos(glm::radians(yaw_)) * cos_pitch,
          sin(glm::radians(pitch_)), sin(glm::radians(yaw_)) * cos_pitch};
      
      return _update_up_(direction);
  }

};
  
#endif
