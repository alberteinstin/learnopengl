//
//  OpenGL
//
//  Created by Albert Einstin on 11/18/19.
//  Copyright © 2019 Albert Einstin. All rights reserved.
//

#include <iostream>
#include <vector>
#include  <array>

#include "glad/glad.h"
#include "GLFW/glfw3.h"


#include "Shader.h"
#include "Texture.h"   // <-  #include "stb_image.h"
#include "Camera.h"

#include "process.h"   // <- processInput,  setupWindow

constexpr unsigned int SCR_WIDTH = 800, SCR_HEIGHT = 600;   //  view port size

Delta g_delta(SCR_WIDTH/2.f, SCR_HEIGHT/2.f);    // keeps track of xy- and frame time deltas
Camera g_cam(glm::vec3(0.0f, 0.0f,  3.0f));  // init position in world
 
int main(int argc, const char * argv[])
{
  const char* const PRG = "main> ";
  using namespace std;
    typedef array<glm::vec3, 3> array_T;

    // for debugging ......
    bool do_debug = true;
    array_T param, prev_param;
    auto p_y_f = g_cam.PitchYawFov(), prev_p_y_f =  p_y_f;
    //.....................
    
  try {
    GLFWwindow* window = setupWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL");
    
    glEnable(GL_DEPTH_TEST); // configure global opengl state
    
      // ................. generating/binding Textures ..................
      Texture diffuse_map(loc_home()+"textures/container_with_steel_border.png");
      auto diffuse_id = diffuse_map.ID();
      Texture specular_map(loc_home()+"textures/container_specular.png");
      auto specular_id = specular_map.ID();
 
     glm::vec3 light_pos(0.0f);       // <<<<<<<<<<<<<<<  light positon (disabled if == vec3(0.f))
// ........... shader processing (compile+link+program on ctor)
    // ----------------------------- cube(s) -------------------------------------
      Shader cube_shader(loc_home()+"shaders/cube_shader_v5.vs","cube_shader_v6.fs");

      cube_shader.use();              // activate/use  before setting uniforms!
      // `material.diffuse' is `uniform' sampler2D in fragment shader
      cube_shader._setLocInt("material.diffuse", 0)._setLocInt("material.specular", 1); //  as texture unit 0 and 1
      cube_shader._setLocFloat("material.shininess", 32.0f);   //
      
      auto cube_model = glm::rotate (glm::mat4(1.0f),glm::radians(20.f), glm::vec3(1.0f, 0.3f, 0.5f));
      cube_shader._setLocMat4("Model",cube_model);
      // set up directional light params
      glm::vec3 light_dir(-0.2f, -1.0f, -0.3f), ambient =  glm::vec3(0.05f), diff =  glm::vec3(0.4f), spec =  glm::vec3(0.5f);   // light direction (caster!)
      cube_shader._setLocVec3("dir_light.light.ambient", ambient)._setLocVec3("dir_light.light.diffuse", diff);
      cube_shader._setLocVec3("dir_light.light.specular", spec)._setLocVec3("dir_light.dir", light_dir);
      // set up common params for all point light sources
      stringstream os;
      auto point_light_pos = point_light_positions();
      const int point_light_nbr = point_light_pos.size();
      for (unsigned int j = 0; j < point_light_nbr; j++)
      {
          os.str(""); os.clear();
          os <<"point_light["<< j << "].";
          auto l = os.str();
          os << l << "light.ambient";      auto s_amb = os.str(); os.str("");os.clear();
          os << l << "light.diffuse";      auto s_diff = os.str(); os.str("");os.clear();
          os << l << "light.specular";      auto s_spec = os.str(); os.str("");os.clear();
          os << l << "constant";      auto s_const = os.str(); os.str("");os.clear();
          os << l << "linear";      auto s_linear = os.str(); os.str("");os.clear();
          os << l << "quadratic";      auto s_quad = os.str(); os.str("");os.clear();
          cube_shader._setLocVec3(s_amb,glm::vec3(0.05f))._setLocVec3(s_diff,glm::vec3(0.8f))._setLocVec3(s_spec, glm::vec3(1.0f));
          cube_shader._setLocFloat(s_const, 1.0f)._setLocFloat(s_linear, 0.09f)._setLocFloat(s_quad, 0.032f);
      }
      // spot light (aka flash light)
      cube_shader._setLocVec3("spot_light.light.diffuse",glm::vec3(1.0f))._setLocVec3("spot_light.light.light.specular", glm::vec3(1.0f));
      cube_shader._setLocFloat("spot_light.cos_cutoff", glm::cos(glm::radians(10.0)))._setLocFloat("spot_light.cos_outer_cutoff", glm::cos(glm::radians(12.0)));
      
      // -------------------------lamp(s) -----------------------------------------
      Shader light_shader(loc_home()+"shaders/cube_shader_v1.vs","light_shader_v1.fs");
      light_shader.use();
      auto light_model = glm::translate(glm::mat4(1.0f),light_pos);  // map to world space
      light_model = glm::scale(light_model, glm::vec3(0.2f));         // a smaller cube
      light_shader._setLocMat4("Model",light_model);
      //      light_shader._setLocVec3("objectColor", 0.2f, 0.5f, 0.31f)._setLocVec3("lightColor",  1.0f, 1.0f, 1.0f);
      // ------------------------------------------------------------------
      // ................
      auto buf = prepLightVertices();     // bring in cube/lamp vertices
      const auto& buf_0 = buf[0], buf_1 = buf[1];
      const auto VERTEX_COUNT = get<0>(buf_0);
      const auto VBO = get<1>(buf_0), cube_VAO = get<2>(buf_0), light_VAO = get<2>(buf_1);
      
      const auto cube_pos = cube_positions();
      const auto cube_nbr = cube_pos.size();
    // ------------  render loop -------------
    while (!glfwWindowShouldClose(window))
    {
        auto curr_time = glfwGetTime();
        g_delta.updateFrameTime(curr_time);
        auto key =  processInput(window, g_cam), prev_key = key; // input from keyboard/mouse
        bool do_spec = get<2>(key), do_diff = get<1>(key), do_ambient = get<0>(key);
        int light_nbr = get<3>(key);
        
        glClearColor(0.2f,0.3f,0.3f,1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear z-buffer
        // ......... debug ...........................
        if (do_debug) {
            p_y_f = g_cam.PitchYawFov();
            param[0] = g_cam.Pos(); param[1] = g_cam.Front(); param[2] = g_cam.Up();
            if (param != prev_param || p_y_f != prev_p_y_f || key != prev_key)
                cout << PRG << "camera " << setprecision(2) << "pos: " << param[0] << ", front: " << param[1] << ", up: " << param[2]
                << ", p_y_f: " << g_cam.PitchYawFov() << (do_ambient?" (amb)":"")<< (do_diff?" (diff)":"")<< (do_spec?" (spec)":"")<< light_nbr << endl;
        }
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, diffuse_id);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, specular_id);
        
        // .............. render target cube (lit) ................
        auto project = glm::perspective(g_cam.FovInRad(), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
        auto view = g_cam.lookAt();
        auto view_pos = g_cam.Pos();
        
        cube_shader.use();
        cube_shader._setLocVec3("viewPos", view_pos);   // pass on view position
        // view/projection transformations
        cube_shader._setLocMat4("Project",project)._setLocMat4("View",view);
        // pass spot light params
        cube_shader._setLocVec3("spot_light.light.pos", view_pos)._setLocVec3("spot_light.dir", g_cam.Front());
        
        cube_shader._setLocBool("amb_on",do_ambient)._setLocBool("diff_on",do_diff)._setLocBool("spec_on",do_spec)._setLocInt("nbr", light_nbr);
        
        for (unsigned int j = 0; j < cube_nbr; j++)
        {
            cube_model = glm::translate(glm::mat4(1.f), cube_pos[j]);
            cube_model = glm::rotate(cube_model, glm::radians(float(cos(curr_time)) * 20.f * (j+2)), glm::vec3(1.0f, 0.3f, 0.5f));
            cube_shader._setLocMat4("Model", cube_model);
            glBindVertexArray(cube_VAO);
            glDrawArrays(GL_TRIANGLES, 0, VERTEX_COUNT);
        }
        
        // .............. render (source of) light cube ................
        light_shader.use();
        // view/projection transformations
        light_shader._setLocMat4("Project",project)._setLocMat4("View",view);
        for (unsigned int j = 0; j < light_nbr; j++)
        {
            auto pos = /*light_pos +*/ point_light_pos[j];
            pos.x *= glm::max (cos(0.3 * curr_time), 0.0);  // move lights in x-z plane (for fun!)
            pos.z *= glm::max(sin(0.3 * curr_time),0.0);
            os.str(""); os.clear();
            os <<"point_light["<< j << "].pos";
            auto s_pos = os.str();
            cube_shader._setLocVec3(s_pos, pos);           // pass on point light position
            
            light_model =glm::translate(glm::mat4(1.f),pos);
            light_model = glm::scale(light_model, glm::vec3(0.1f));         // a smaller cube
            light_shader._setLocMat4("Model", light_model);
            // render each
            glBindVertexArray(light_VAO);
            glDrawArrays(GL_TRIANGLES, 0, VERTEX_COUNT);
            
        }
        
        // glBindVertexArray(0); // no need to unbind it every time
        glfwSwapBuffers(window);
        glfwPollEvents();
        
        // ......... debug ...........................
        if (do_debug) {
            prev_param = param;
            prev_p_y_f = p_y_f;
            prev_key = key;
        }
        // ....................................
    }
    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &light_VAO);
    glDeleteVertexArrays(1, &cube_VAO);
    glDeleteBuffers(1, &VBO);
  
    
    glfwTerminate();
    return 0;
  }
  catch (const std::string& e) {
    std::cerr << PRG << e << std::endl;
  }
  catch (const std::system_error& e) {
    std::cerr << PRG << e.what() << " (" << e.code() << ")" << std::endl;
  }
  catch (...) {
    std::cerr << PRG << " caught an unknown exception" << std::endl;
  }
    glfwTerminate();
    return -1;
}
