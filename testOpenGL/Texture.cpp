//
 
//  OpenGL
//
//  Created by Albert Einstin on 11/18/19.
//  Copyright © 2019 Albert Einstin. All rights reserved.
//
#include "glad/glad.h"
#include "GLFW/glfw3.h"

#include <stdio.h>
#include <system_error>
#include <exception>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "Texture.h"

 const char* Texture::TypeName[int(eType::eSize)] = {"material.diffuse","material.specular","material.normal","material.height","material.ambient"};

void Texture::load_(const std::string& texturePath, bool doFlip /*=true*/)
{
  glGenTextures(1, &id_);
  
  if (doFlip)
    stbi_set_flip_vertically_on_load(true);
  
  int width, height, nrChannels;
  unsigned char *p_data = stbi_load(texturePath.c_str(), &width, &height, &nrChannels, 0);
  if (!p_data)
    throw std::system_error(errno,std::system_category(),"Texture::> failed to open/load texture: "+texturePath);

  GLenum format;
  if (nrChannels == 1)
    format = GL_RED;
  else if (nrChannels == 3)
    format = GL_RGB;
  else if (nrChannels == 4)
    format = GL_RGBA;
     
  // load and generate the texture
  glBindTexture(GL_TEXTURE_2D,id_);
    //a grap a chunk of memory on the graphics card
  glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, p_data);
  glGenerateMipmap(GL_TEXTURE_2D);
  
  // set the texture wrapping/filtering options (on the currently bound texture object)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  
  stbi_image_free(p_data);
    // it has to be called to release mem
    // glDeleteTextures(1, &id_) maybe called to release graphics mem
}
