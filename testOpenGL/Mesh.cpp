
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Mesh.h"

using namespace mesh;
using namespace std;
//static
unsigned int Mesh::s_instance_idx_ {0};
std::map<unsigned long, unsigned int> Mesh::s_map_instance_;


void
Mesh::draw(const Shader& MyShader, bool doDebug /*= false*/) const
{
    const char* const PRG  = "Mesh::draw>";
    if (!textures_.size())
        throw std::string("Mesh::draw> no textures to draw with! bailing ....");
    // bind appropriate textures
    unsigned int diffuseNr  = 0, specularNr = 0;
    unsigned int normalNr   = 0, heightNr   = 0, ambientNr = 0;
    // .......... for debug info ...........
    vector<tuple<string,int,int>> d_track;
    // .....................................
    for(unsigned int i = 0; i < textures_.size(); i++)
    {
        const auto & curr_texture = textures_[i];
        glActiveTexture(GL_TEXTURE0 + i); // active proper texture unit before binding
        // retrieve texture number (the N in diffuse_textureN)
        string number;
        const auto type = curr_texture->Type();
        if (type == Texture::eType::eDiffuse)
            number = std::to_string(diffuseNr++);
        else if(type == Texture::eType::eSpecular)
            number = std::to_string(specularNr++); // transfer unsigned int to stream
        else if(type == Texture::eType::eNormal)
            number = std::to_string(normalNr++); // transfer unsigned int to stream
        else if(type == Texture::eType::eHeight)
            number = std::to_string(heightNr++);
        else if(type == Texture::eType::eAmbient)
            number = std::to_string(ambientNr++);
        else
            throw std::string("Mesh::draw> unknown texture type!");
        
        std::string tex_name = Texture::TypeName[int(type)] + number;  // <== convention for texture name, e.g. "material.diffuse<i>"
        MyShader.use();
        MyShader._setLocInt(tex_name, i);                         // set the sampler to the correct texture unit
        if (doDebug  && do_first_time_)
            d_track.push_back(make_tuple(tex_name,i,curr_texture->ID()));
        
        glActiveTexture(GL_TEXTURE0+i);               // <ag>   
        // and finally bind the texture
        glBindTexture(GL_TEXTURE_2D, curr_texture->ID());
    }
     // .......... for debug info ...........
    if (doDebug && do_first_time_) {
        if (d_track.size() != textures_.size())
            throw std::string("Mesh::draw> inconsistent nbr of textures!");
        unsigned int j = 0;
        cout << PRG << "("<< s_map_instance_[my_this_]<< ") "<< get<0>(d_track[j]) << ", u:"<< get<1>(d_track[j]) << ", id: " << get<2>(d_track[j]) << endl;
        for (j = 1; j<  d_track.size(); j++ )
            cout << PRG << "("<< s_map_instance_[my_this_]<< ") "<< get<0>(d_track[j]) << ", u:"<< get<1>(d_track[j]) << ", id: " << get<2>(d_track[j]) << endl;
        cout << endl;
        do_first_time_ = false;
    }
    // draw mesh
    glBindVertexArray(VAO_);
    glDrawElements(GL_TRIANGLES, indices_.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
    
    // always good practice to set everything back to defaults once configured.
    glActiveTexture(GL_TEXTURE0);
}

void
Mesh::setup_mesh_() const
{
  // create buffers/arrays
  glGenVertexArrays(1, &VAO_);
  glGenBuffers(1, &VBO_);
  glGenBuffers(1, &EBO_);
  
  glBindVertexArray(VAO_);
  // load data into vertex buffers
  glBindBuffer(GL_ARRAY_BUFFER, VBO_);
  // A great thing about structs is that their memory layout is sequential for all its items.
  // The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
  // again translates to 3/2 floats which translates to a byte array.
  glBufferData(GL_ARRAY_BUFFER, vertices_.size() * sizeof(Vertex), &vertices_[0], GL_STATIC_DRAW);  

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO_);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_.size() * sizeof(unsigned int), &indices_[0], GL_STATIC_DRAW);

  // set the vertex attribute pointers
  // vertex Positions
  glEnableVertexAttribArray(0);	
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
  // vertex normals
  glEnableVertexAttribArray(1);	
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
  // vertex texture coords
  glEnableVertexAttribArray(2);	
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoords));
        // vertex tangent
  glEnableVertexAttribArray(3);
  glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, tangent));
  // vertex bitangent
  glEnableVertexAttribArray(4);
  glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, bitangent));
  
  glBindVertexArray(0);
}

void
Mesh::clear ()
{
    std::for_each(textures_.begin(), textures_.end(), [](auto x){x->clear();});
    indices_.resize(0);
    vertices_.resize(0);
    if (VAO_)
        glDeleteVertexArrays(1, &VAO_);
    if (VBO_)
        glDeleteBuffers(1, &VBO_);
    if (EBO_)
        glDeleteBuffers(1, &EBO_);
    
    
}
