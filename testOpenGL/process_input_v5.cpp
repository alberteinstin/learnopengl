#include <vector>

#include "util.h"  // clamp
#include "process.h"
 
extern Delta g_delta;
extern Camera g_cam;


std::tuple<bool,bool,bool,int>
processInput (GLFWwindow* pWindow, Camera& Cam)
{
  const char *const PRG = "processInput> ";

  using namespace std;
    
   
    if (glfwGetKey(pWindow, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(pWindow, true);

     static unsigned char wireframe = 0;
     if (glfwGetKey(pWindow, GLFW_KEY_W) == GLFW_PRESS)
       glPolygonMode(GL_FRONT_AND_BACK, (wireframe = 1 - wireframe) ? GL_LINE : GL_FILL);

    const float cameraSpeed = 2.5f * g_delta.Time();            // = 0.05f; // adjust accordingly
    if (glfwGetKey(pWindow, GLFW_KEY_E) == GLFW_PRESS) {
      Cam._zoom(cameraSpeed);
    }
    if (glfwGetKey(pWindow, GLFW_KEY_S) == GLFW_PRESS) {
      Cam._zoom(-cameraSpeed);
    }
    if (glfwGetKey(pWindow, GLFW_KEY_L) == GLFW_PRESS) {
      Cam._strafe(-cameraSpeed);
    }
    if (glfwGetKey(pWindow, GLFW_KEY_R) == GLFW_PRESS) {
      Cam._strafe(cameraSpeed);
    }
    if (glfwGetKey(pWindow, GLFW_KEY_Q) == GLFW_PRESS) {
        Cam._reset(glm::vec3(0.f,0.f,3.f));
    }
    
    static bool do_spec = true, do_diff = true, do_ambient = true;
    static int nbr = 4;
    if (glfwGetKey(pWindow, GLFW_KEY_Y) == GLFW_PRESS) {
         do_ambient = !do_ambient;
       }
    if (glfwGetKey(pWindow, GLFW_KEY_U) == GLFW_PRESS) {
           do_diff = !do_diff;
         }
    if (glfwGetKey(pWindow, GLFW_KEY_I) == GLFW_PRESS) {
           do_spec = !do_spec;
         }
    if (glfwGetKey(pWindow, GLFW_KEY_0) == GLFW_PRESS) {
           nbr = 0;
         }
    if (glfwGetKey(pWindow, GLFW_KEY_1) == GLFW_PRESS) {
           nbr = 1;
         }
    if (glfwGetKey(pWindow, GLFW_KEY_2) == GLFW_PRESS) {
            nbr = 2;
          }
    if (glfwGetKey(pWindow, GLFW_KEY_3) == GLFW_PRESS) {
            nbr = 3;
          }
    if (glfwGetKey(pWindow, GLFW_KEY_4) == GLFW_PRESS) {
            nbr = 4;
          }

    return tuple<bool,bool,bool,int>(do_ambient,do_diff,do_spec,nbr);
//    if (glfwGetKey(pWindow, GLFW_) == GLFW_PRESS) {
//      Cam._strafe(-cameraSpeed);
//    }
}

// used with glut. 
void special_keys(int key, int x, int y)
{
        const float cameraSpeed = 2.5f * g_delta.Time();            // = 0.05f; // adjust accordingly
    switch (key)
    {
        case GLUT_KEY_LEFT:
            g_cam._strafe(-cameraSpeed);
            break;
        case GLUT_KEY_RIGHT:
 //           doSomething();
            break;
        case GLUT_KEY_UP:
 //           doSomething();
            break;
        case GLUT_KEY_DOWN:
    // doSomething();
            break;
    }
}


void processInput (GLFWwindow* pWindow)
{
    if (glfwGetKey(pWindow, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(pWindow, true);

    static unsigned char wireframe = 0;

    if (glfwGetKey(pWindow, GLFW_KEY_W) == GLFW_PRESS)
      glPolygonMode(GL_FRONT_AND_BACK, (wireframe = 1 - wireframe) ? GL_LINE : GL_FILL);

}


GLFWwindow* setupWindow (int Width, int Height, const char* Name)
{
  using namespace std;
  glfwInit();
  
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,3);
  glfwWindowHint(GLFW_OPENGL_PROFILE,GLFW_OPENGL_CORE_PROFILE);
  
#ifdef __APPLE__
  // uncomment this statement to fix compilation on OS X
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
  
  GLFWwindow* p_window = glfwCreateWindow(Width, Height, Name, NULL, NULL);
  if (!p_window) {
    std::cout << "Failed to create GLFW window" << std::endl;
//      glfwTerminate();
    throw "Failed to create GLFW window";
  }
  glfwMakeContextCurrent(p_window);
  // this call connect opengl driver particular to this machine
  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
    cout << "Failed to initialize GLAD" << endl;
    throw "Failed to initialize GLAD";
  }
  glViewport(0,0,Width,Height);  // size of gl rendering window
    // mouse cursor stays within the center of the window
  // glfwSetInputMode(p_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    
  // ... register callbacks ......
  glfwSetFramebufferSizeCallback(p_window, framebuffer_size_callback); // register
  glfwSetCursorPosCallback(p_window, mouse_callback);
  glfwSetScrollCallback(p_window, scroll_callback);
  
  return p_window;
}

void framebuffer_size_callback(GLFWwindow* p_window, int w, int h)
{
    glViewport(0,0,w,h);
}
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
   g_cam._updateFov(-yoffset,true);
    
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    static bool is_first_time = true;
    auto offset = g_delta.updateXY(xpos, ypos, is_first_time);
 
    constexpr float sensitivity = 0.2f;

    auto xoffset = offset.first * sensitivity;
    auto yoffset = offset.second * sensitivity;

  g_cam._updateYaw(xoffset)._updatePitch(yoffset,true);
 
 }  

std::array<glm::vec3,10>
cube_positions ()
{
  std::array<glm::vec3,10>  pos  = {
    glm::vec3( 0.0f,  0.0f,  0.0f), 
    glm::vec3( 2.0f,  5.0f, -15.0f), 
    glm::vec3(-1.5f, -2.2f, -2.5f),  
    glm::vec3(-3.8f, -2.0f, -12.3f),  
    glm::vec3( 2.4f, -0.4f, -3.5f),  
    glm::vec3(-1.7f,  3.0f, -7.5f),  
    glm::vec3( 1.3f, -2.0f, -2.5f),  
    glm::vec3( 1.5f,  2.0f, -2.5f), 
    glm::vec3( 1.5f,  0.2f, -1.5f), 
    glm::vec3(-1.3f,  1.0f, -1.5f)  
  };
  return pos;
}

std::array<glm::vec3,4>
point_light_positions ()
{
std::array<glm::vec3,4>  pos = {
    glm::vec3( 0.7f,  0.2f,  2.0f),
    glm::vec3( 2.3f, -3.3f, -4.0f),
    glm::vec3(-4.0f,  2.0f, -12.0f),
    glm::vec3( 0.0f,  3.0f, -3.0f)
};
    return pos;
}
