//
 
//  OpenGL
//
//  Created by Albert Einstin on 11/18/19.
//  Copyright © 2019 Albert Einstin. All rights reserved.
//

#include <stdio.h>
#include <exception>

#include "Shader.h"
// -> 'fragmentPath' can be just the file name if it shares same dir_path as 'vertexPath'
Shader::Shader(const std::string& vertexPath, std::string fragmentPath, const std::string& geometryPath /*= ""*/)
{
    // 1. retrieve the vertex/fragment source code from filePath
  std::string vertexCode, fragmentCode, geometryCode;
  std::ifstream vShaderFile, fShaderFile, gShaderFile;
    // ensure ifstream objects can throw exceptions:
    //    vShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);

    try
    {
        auto idx =  vertexPath.find_last_of("/");
        std::string dir = idx > 0 ? vertexPath.substr(0,idx) : "";
        
       vShaderFile.open(vertexPath); // open files
      if (!vShaderFile)
        throw std::system_error(errno,std::system_category(),"Shader::> failed to open vertex shader: "+vertexPath);

        idx =  fragmentPath.find_last_of("/");
        if (idx == std::string::npos) // it's just a name
            fragmentPath = dir + "/" + fragmentPath;
      
      fShaderFile.open(fragmentPath);
     if (!fShaderFile)
        throw std::system_error(errno,std::system_category(),"Shader::> failed to open fragment shader: "+fragmentPath);
     std::stringstream vShaderStream, fShaderStream;
     vShaderStream << vShaderFile.rdbuf();     // read file's buffer contents into streams
     fShaderStream << fShaderFile.rdbuf();

      vShaderFile.close();      // close file handlers
      fShaderFile.close();
      
      vertexCode   = vShaderStream.str();      // convert stream into string
      fragmentCode = fShaderStream.str();
      
      if (geometryPath != "") {
        gShaderFile.open(geometryPath); // open files
        if (!gShaderFile)
          throw std::system_error(errno,std::system_category(),"Shader::> failed to open geometry shader: "+geometryPath);
         std::stringstream gShaderStream;
        gShaderStream << gShaderFile.rdbuf();         // read file's buffer contents into streams
        gShaderFile.close();// close file handlers
      }
    }
    // this catch won't happen unless enabled `vShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);'
     catch(std::ifstream::failure e)
    {
         std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
    }
    const char* vShaderCode = vertexCode.c_str();
    const char* fShaderCode = fragmentCode.c_str();
    // 2. compile shaders
    unsigned int vertex, fragment, geometry;
       
    // vertex Shader
    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderCode, NULL);
    glCompileShader(vertex);
    if (!check_compile_errors_(vertex,"VERTEX"))    // print compile errors if any
      throw std::string("Shader::> could not compile VERTEX shader: ") + vertexPath;
    // fragment Shader
    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fShaderCode, NULL);
    glCompileShader(fragment);
    if (!check_compile_errors_(fragment,"FRAGMENT")) // print compile errors if any
     throw std::string("Shader::> could not compile FRAGMENT shader: ") + fragmentPath;
  
    if (geometryPath != "") {    // geometry Shader
      const char* gShaderCode = geometryCode.c_str();
      geometry = glCreateShader(GL_GEOMETRY_SHADER);
      glShaderSource(geometry, 1, &gShaderCode, NULL);
      glCompileShader(geometry);
      if (!check_compile_errors_(geometry,"GEOMETRY")) // print compile errors if any
        throw std::string("Shader::> could not compile GEOMETRY shader") + geometryPath;
  }
    // shader Program
    ID_ = glCreateProgram();
    glAttachShader(ID_, vertex);
    glAttachShader(ID_, fragment);
    if (geometryPath != "") {
      glAttachShader(ID_, geometry);
    }
    glLinkProgram(ID_);
    check_compile_errors_(ID_,"PROGRAM"); // print linking errors if any
        
    // delete the shaders as they're linked into our program now and no longer necessery
    glDeleteShader(vertex);
    glDeleteShader(fragment);
    if (geometryPath != "")
      glDeleteShader(geometry);
     
}

GLint
Shader::check_compile_errors_(GLuint shader, std::string type)
{
  GLint success;
  GLchar infoLog[1024];
  if(type != "PROGRAM")
  {
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if(!success)
    {
      glGetShaderInfoLog(shader, 1024, NULL, infoLog);
      std::cout << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
    }
  }
  else
  {
    glGetProgramiv(shader, GL_LINK_STATUS, &success);
    if(!success)
    {
      glGetProgramInfoLog(shader, 1024, NULL, infoLog);
      std::cout << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
    }
  }
  return success;
}
 
