#ifndef process_H
#define process_H

#include <iostream>
#include <vector>
#include <tuple>

#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include <glm/glm.hpp>
#include <GLUT/glut.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Camera.h" 

inline auto loc_home() {
    return std::string("/Users/alb/Albert/xcode/testOpenGL/");
}
// .......declarations
void framebuffer_size_callback(GLFWwindow*, int w, int h);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);

std::array<glm::vec3,10> cube_positions ();
std::array<glm::vec3,4>  point_light_positions ();

void special_keys(int key, int x, int y);

void processInput (GLFWwindow* pWindow);
std::tuple<bool,bool,bool,int>  processInput (GLFWwindow* pWindow, Camera& Cam);

GLFWwindow* setupWindow (int Width, int Hight, const char* Name = "No Name");

//  vertex_count, VBO, VAO, EBO;
std::tuple<unsigned int,unsigned int,unsigned int, unsigned int> prepVertices ();
// two boxes
std::vector<std::tuple<unsigned int,unsigned int,unsigned int>> prepLightVertices ();


inline std::ostream& operator << (std::ostream& os, const glm::vec3& rhs) {
  os << "[" << rhs[0] << "," << rhs[1] << "," << rhs[2] << "]";
    return os;
}


template <typename T, size_t N>
std::ostream& operator << (std::ostream& os, const std::array<T,N>& rhs) {
    os << "{" << rhs[0];
    for (size_t i = 1; i < N; i++ )
        os << "," << rhs[i];
        os << "}";
    return os;
}

struct Delta {
  Delta(float xPos, float yPos): lastX {xPos}, lastY {yPos} {
  }
     float delta_time {0.f};// Time between current frame and last frame
    float lastFrame {0.f};// Time of last frame
    float lastX {0.f};
    float lastY {0.f};

  auto Time () const { return delta_time; }
  
  std::pair<float,float>
  updateXY (float xPos, float yPos, bool& isFirstTime)
    {
      if (isFirstTime) {
        lastX = xPos;
        lastY = yPos;
        isFirstTime = false;
      }
      float xoffset = xPos - lastX;
      float yoffset = lastY - yPos; 
      lastX = xPos;
      lastY = yPos;
      
      return std::make_pair(xoffset,yoffset);
    }

  void updateFrameTime (float FrameTime) {
    delta_time = FrameTime - lastFrame;
    lastFrame = FrameTime;
  }
};

#endif
