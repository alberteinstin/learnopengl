//
//  OpenGL
//
//  Created by Albert Einstin on 11/18/19.
//  Copyright © 2019 Albert Einstin. All rights reserved.
//

#include <iostream>
#include <vector>
#include "glad/glad.h"
#include "GLFW/glfw3.h"


#include "Model.h"
#include "Camera.h"

#include "process.h"   // <- processInput,  setupWindow, Delta


const unsigned int SCR_WIDTH = 800, SCR_HEIGHT = 600;   //  view port size

Delta g_delta(SCR_WIDTH/2.f, SCR_HEIGHT/2.f);    // keeps track of xy- and frame time deltas
Camera g_cam(glm::vec3(0.0f, 0.0f,  3.0f));  // init position in world
 
int main(int argc, const char * argv[])
{
    const char* const PRG = "main> ";
    using namespace std;
     typedef array<glm::vec3, 3> array_T;
    
    glutSpecialFunc(special_keys);   // arrow keys
    
 
    glm::vec3 light_pos(1.2f, 1.0f, 2.0f); // light positon
        // for debugging ......
    bool do_debug = true, do_debug_render = false;
    array_T param, prev_param;
    auto p_y_f = g_cam.PitchYawFov(), prev_p_y_f =  p_y_f;
    //.....................

    auto model = glm::mat4(1.0f);
    try {
        GLFWwindow* window = setupWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL");
        
        glEnable(GL_DEPTH_TEST); // configure global opengl state
        
        // ........... shader processing (compile+link+program on ctor)
        Shader shader(loc_home()+"shaders/model_loading_v1.vs","model_loading_v1.fs");
        shader.use();
          shader._setLocFloat("material.shininess", 8.0f);
            //    shader._setLocVec3("light.pos", light_pos);
        shader._setLocVec3("light.ambient",glm::vec3(0.1f))._setLocVec3("light.diffuse",glm::vec3(1.0f))._setLocVec3("light.specular", glm::vec3(0.3f));
        // .............. 3d model loading  .....................................
        cout << PRG << "shader(s) loaded, loading model: ...." << endl;
       Model backpack(loc_home()+"../3d_models/backpack/backpack.obj", do_debug);        // <<<<_______ loading the 3d model
        cout << PRG << "....model loaded ---> good to go" << endl;
        
        // ------------  render loop --------------------------------------------------------------------------
        while (!glfwWindowShouldClose(window)) {
            
          auto curr_time = glfwGetTime();
          g_delta.updateFrameTime(curr_time);
          auto key =  processInput(window, g_cam); // input from keyboard/mouse
          bool do_spec = get<2>(key), do_diff = get<1>(key), do_ambient = get<0>(key);
          
          
          glClearColor(0.2f,0.3f,0.3f,1.0f);
          glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear z-buffer
          
          // view/projection transformations
          // .............. render target cube (lit) ................
          auto project = glm::perspective(g_cam.FovInRad(), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
          auto view = g_cam.lookAt();
          auto pos = g_cam.Pos();
          shader.use();
          shader._setUniformLocMat4("View",view)._setUniformLocMat4("Project",project)._setUniformLocMat4("Model", model);
          shader._setUniformLocVec3("viewPos", pos)._setUniformLocVec3("lightPos", light_pos);
          shader._setLocBool("amb_on",do_ambient)._setLocBool("diff_on",do_diff)._setLocBool("spec_on",do_spec);
                  // ......... debug ...........................
        if (do_debug_render) {
            p_y_f = g_cam.PitchYawFov();
            param[0] = g_cam.Pos(); param[1] = g_cam.Front(); param[2] = g_cam.Up();
            if (param != prev_param || p_y_f != prev_p_y_f)
                cout << PRG << "camera " << setprecision(2) << "pos: " << param[0] << ", front: " << param[1] << ", up: " << param[2]
                << ", p_y_f: " << g_cam.PitchYawFov() << (do_ambient?" (amb)":"")<< (do_diff?" (diff)":"")<< (do_spec?" (spec)":"")<< endl;
        }
 
            
            backpack.draw(shader);      // <<<=== draw model
            
            // glBindVertexArray(0); // no need to unbind it every time
            glfwSwapBuffers(window);
            glfwPollEvents();
                    // ......... debug ...........................
        if (do_debug_render) {
            prev_param = param;
            prev_p_y_f = p_y_f;
        }
        // ....................................

        }
        
        backpack.clear();  // release resources!
        
        glfwTerminate();
        return 0;
    }
    catch (const std::string& e) {
        std::cerr << PRG << e << std::endl;
    }
    catch (const std::system_error& e) {
        std::cerr << PRG << e.what() << " (" << e.code() << ")" << std::endl;
    }
    catch (...) {
        std::cerr << PRG << " caught an unknown exception" << std::endl;
    }
    glfwTerminate();
    return -1;
}
