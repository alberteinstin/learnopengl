#ifndef MESH_H
#define MESH_H

#include <string>
#include <vector>
#include <map>
#include <glad/glad.h> // holds all OpenGL type declarations

#include "Shader.h" 
#include "Texture.h" 

namespace mesh
{
  struct Vertex
  {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 texCoords;
    glm::vec3 tangent;
    glm::vec3 bitangent;
  };
  
  
  class Mesh
{
    typedef Mesh SELF;
public:
    typedef  std::vector<Vertex>       v_T;
    typedef  std::vector<std::shared_ptr<Texture>>      t_T;
    typedef  std::vector<unsigned int> i_T;
    typedef size_t                  this_T;
private:
    // mesh Data
    v_T       vertices_;
    i_T       indices_;
    t_T       textures_;
      mutable unsigned int      VAO_ {0}, VBO_ {0}, EBO_ {0};
    // for debugging
    mutable bool               do_first_time_{true};          // enable debug info on 1st pass only!
    this_T                     my_this_{0};
   
public:
      // move ctor
    Mesh(v_T&& Vertices, i_T&& Indices, t_T&& Textures):
      vertices_{std::move(Vertices)},  indices_{std::move(Indices)},  textures_{std::move(Textures)}
      {
        setup_mesh_();
          // register instance (for debug/stat info gathering)
        s_map_instance_[my_this_= (this_T)this] = s_instance_idx_++;   // keep track of # of instances!
      }
      Mesh(const v_T& Vertices = v_T(0), const i_T& Indices = i_T(0), const t_T& Textures = t_T(0)):
     vertices_{Vertices},  indices_{Indices},  textures_{Textures}
     {
       setup_mesh_();
         // register instance (for debug/stat info gathering)
       s_map_instance_[my_this_= (this_T)this] = s_instance_idx_++;    // keep track of # of instances!
     }
    Mesh (const SELF& rhs) {
        init_(rhs);
    }
      Mesh (SELF&& rhs) {
           init_(rhs);
          // register instance (for debug/stat info gathering)
           s_map_instance_[my_this_= (this_T)this] = s_instance_idx_++;
       }
    
    SELF& operator = (const SELF& rhs) {
        if (this == &rhs)
            return *this;
        return init_(rhs);
    }
    SELF& operator = (SELF&& rhs) {
        if (this == &rhs)
            return *this;
        return init_(rhs);
    }

    // render the mesh
    void draw(const Shader& shader, bool doDebug = false) const;
      
      void clear ();   // release resouces
 

  private:
    SELF& init_(const SELF& rhs) {
        vertices_ = rhs.vertices_;
        indices_ = rhs.indices_;
        textures_ = rhs.textures_;
        VAO_ = rhs.VAO_;
        VBO_ = rhs.VBO_;
        EBO_ = rhs.EBO_;
        do_first_time_ = rhs.do_first_time_;
        my_this_      = rhs.my_this_;
        return *this;
    }
      SELF& init_(SELF&& rhs) {
           vertices_ = rhs.vertices_;
           indices_ = rhs.indices_;
           textures_ = rhs.textures_;
           VAO_ = rhs.VAO_;
           VBO_ = rhs.VBO_;
           EBO_ = rhs.EBO_;
           do_first_time_ = rhs.do_first_time_;
           my_this_      = rhs.my_this_;
           return *this;
       }
    
    void setup_mesh_() const;
    static unsigned int s_instance_idx_;
    static std::map<unsigned long, unsigned int> s_map_instance_;
  };
}
#endif
