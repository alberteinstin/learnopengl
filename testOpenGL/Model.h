#ifndef MODEL_H_
#define MODEL_H_

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Mesh.h" 


class Model 
{
    typedef std::vector<std::shared_ptr<mesh::Mesh>> meshes_T;
public:
    // loads a model located at `Path'
    Model(const std::string& Path, bool doDebug = false): gamma_correction_{1}, do_debug_{doDebug}
    {
        load_model_(Path);
    }
    ~Model () { clear(); }   // release resources
    
    void draw(const Shader& shader) const;
    void clear ();                              // release resources
    
private:
    // model data
    mesh::Mesh::t_T   textures_loaded_;      // these are shared_ptr copies
    meshes_T          meshes_;
    std::string       directory_;
    bool              gamma_correction_;
    bool              do_debug_ {false};                     // enable/disable internal debugging info
    mutable bool      do_first_time_{true};          // enable debug info on 1st pass only!
    
    void load_model_(const std::string& path);
    
    void process_node_(aiNode *node, const aiScene *scene);
    std::shared_ptr<mesh::Mesh> process_mesh_(aiMesh *mesh, const aiScene *scene);
    
  std::vector<std::shared_ptr<Texture>> load_material_textures_(aiMaterial *mat, aiTextureType type, Texture::eType texType);
    
};

#endif
