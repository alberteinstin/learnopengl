//
 //  OpenGL
//
//  Created by Albert Einstin on 11/18/19.
//  Copyright © 2019 Albert Einstin. All rights reserved.
//

#ifndef TEXTURE_H
#define TEXTURE_H

#include <glad/glad.h> // include glad to get all the required OpenGL headers
  
#include <string>
 
class Texture
{
 public:
  enum class eType {
    eDiffuse, eSpecular, eNormal, eHeight, eAmbient, eSize
  };
    static const char* TypeName[int(eType::eSize)]; // = {"texture_diffuse","texture_specular","texture_normal","texture_height","texture_ambient"};

public:

  // reads and builds the texture
  Texture(const std::string& PathName  = "", bool doFlip=true) : type_{eType::eDiffuse} {
      if (PathName != "") {
      load_(PathName, doFlip);   // may throw
      auto idx =  PathName.find_last_of("/");
      path_ = idx > 0 ? PathName.substr(0,idx) : "";
          name_= idx > 0 ? PathName.substr(idx+1) : PathName;
          
      }
  }
  Texture(const std::string& Name, const std::string& Dir, eType texType = eType::eDiffuse, bool doFlip=true): type_{texType}
    {
      if (Dir != "" &&  Name != "") {
      auto path = Dir + "/" + Name;
      load_(path, doFlip);// may throw
          path_ = Dir;
          name_ = Name;
      }
    }
    
    ~Texture () {
        clear();              //maybe called to release graphics mem
    }

  auto ID () const { return id_; }          // a handle into graphics subsystem mem
  auto Type() const { return type_; }
  auto Path () const { return path_; }      // dir part of full path_name
  auto Name () const { return name_;}
  
    void clear () {
        if (id_)
            glDeleteTextures(1, &id_); // maybe called to release graphics mem
        id_ = 0;                       // prevents double release
    }
private:
   void load_(const std::string& texturePath, bool doFlip=true);

private:
    unsigned int id_ {0};             // is assigned on `load_'
    std::string path_ {""}, name_ {""};
    enum eType type_  {eType::eSize};

};
  
#endif
