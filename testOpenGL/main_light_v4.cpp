//
//  OpenGL
//
//  Created by Albert Einstin on 11/18/19.
//  Copyright © 2019 Albert Einstin. All rights reserved.
//

#include <iostream>
#include <vector>
#include  <array>

#include "glad/glad.h"
#include "GLFW/glfw3.h"


#include "Shader.h"
#include "Texture.h"   // <-  #include "stb_image.h"
#include "Camera.h"

#include "process.h"   // <- processInput,  setupWindow

constexpr unsigned int SCR_WIDTH = 800, SCR_HEIGHT = 600;   //  view port size

Delta g_delta(SCR_WIDTH/2.f, SCR_HEIGHT/2.f);    // keeps track of xy- and frame time deltas
Camera g_cam(glm::vec3(0.0f, 0.0f,  3.0f));  // init position in world
 
int main(int argc, const char * argv[])
{
  const char* const PRG = "main> ";
  using namespace std;
    typedef array<glm::vec3, 3> array_T;

    glm::vec3 light_pos(1.2f, 1.0f, 2.0f); // <<<<<<<<<<<<<<<  light positon
    // for debugging ......
    bool do_debug = true;
    array_T param, prev_param;
    auto p_y_f = g_cam.PitchYawFov(), prev_p_y_f =  p_y_f;
    //.....................
    
  try {
    GLFWwindow* window = setupWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL");
    
    glEnable(GL_DEPTH_TEST); // configure global opengl state
    
      // ................. generating/binding Textures ..................
      Texture diffuse_map(loc_home()+"textures/container_with_steel_border.png");
      auto diffuse_id = diffuse_map.ID();
      Texture specular_map(loc_home()+"textures/container_specular.png");
      auto specular_id = specular_map.ID();
 
// ........... shader processing (compile+link+program on ctor)
    // ------------------------------------------------------------------
      Shader cube_shader(loc_home()+"shaders/cube_shader_v5.vs","cube_shader_v5.fs");

      cube_shader.use();// activate/use  before setting uniforms!
      // `material.diffuse' is `uniform' sampler2D in fragment shader
      cube_shader._setLocInt("material.diffuse", 0)._setLocInt("material.specular", 1); //  as texture unit 0 and 1
      cube_shader._setLocFloat("material.shininess", 2.0f);   //
      
      auto cube_model = glm::rotate (glm::mat4(1.0f),glm::radians(20.f), glm::vec3(1.0f, 0.3f, 0.5f));
      cube_shader._setLocMat4("Model",cube_model)._setLocVec3("light.pos", light_pos);
      cube_shader._setLocVec3("light.ambient",glm::vec3(0.1f))._setLocVec3("light.diffuse",glm::vec3(0.5f))._setLocVec3("light.specular", glm::vec3(1.0f));

      // ------------------------------------------------------------------
      Shader light_shader(loc_home()+"shaders/cube_shader_v1.vs","light_shader_v1.fs");
      light_shader.use();
      auto light_model = glm::translate(glm::mat4(1.0f),light_pos);  // map to world space
      light_model = glm::scale(light_model, glm::vec3(0.2f));         // a smaller cube
      light_shader._setLocMat4("Model",light_model);
      //      light_shader._setLocVec3("objectColor", 0.2f, 0.5f, 0.31f)._setLocVec3("lightColor",  1.0f, 1.0f, 1.0f);
      // ------------------------------------------------------------------
      // ................
      auto buf = prepLightVertices(); //
      const auto& buf_0 = buf[0], buf_1 = buf[1];
      const auto VERTEX_COUNT = get<0>(buf_0);
      const auto VBO = get<1>(buf_0), cube_VAO = get<2>(buf_0), light_VAO = get<2>(buf_1);
      
      auto cube_pos = cube_positions();
      
    // ------------  render loop -------------
    while (!glfwWindowShouldClose(window))
    {
        auto curr_time = glfwGetTime();
        g_delta.updateFrameTime(curr_time);
        auto key =  processInput(window, g_cam); // input from keyboard/mouse
        bool do_spec = get<2>(key), do_diff = get<1>(key), do_ambient = get<0>(key);
        
        glClearColor(0.2f,0.3f,0.3f,1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear z-buffer
        // ......... debug ...........................
        if (do_debug) {
            p_y_f = g_cam.PitchYawFov();
            param[0] = g_cam.Pos(); param[1] = g_cam.Front(); param[2] = g_cam.Up();
            if (param != prev_param || p_y_f != prev_p_y_f)
                cout << PRG << "camera " << setprecision(2) << "pos: " << param[0] << ", front: " << param[1] << ", up: " << param[2]
                << ", p_y_f: " << g_cam.PitchYawFov() << (do_ambient?" (amb)":"")<< (do_diff?" (diff)":"")<< (do_spec?" (spec)":"")<< endl;
        }
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, diffuse_id);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, specular_id);
        
        // .............. render target cube (lit) ................
        auto project = glm::perspective(g_cam.FovInRad(), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
        auto view = g_cam.lookAt();
        auto view_pos = g_cam.Pos();
        
        cube_shader.use();
        cube_shader._setLocVec3("viewPos", view_pos);   // pass on view position
        // view/projection transformations
        cube_shader._setLocMat4("Project",project)._setLocMat4("View",view);
        cube_shader._setLocBool("amb_on",do_ambient)._setLocBool("diff_on",do_diff)._setLocBool("spec_on",do_spec);
        
        for (unsigned int j = 0; j < cube_pos.size(); j++)
        {
            cube_model = glm::translate(glm::mat4(1.f), cube_pos[j]);
            cube_model = glm::rotate(cube_model, glm::radians(float(cos(curr_time)) * 20.f * j), glm::vec3(1.0f, 0.3f, 0.5f));
            cube_shader._setLocMat4("Model", cube_model);
            glBindVertexArray(cube_VAO);
            glDrawArrays(GL_TRIANGLES, 0, VERTEX_COUNT);
        }
        
        // .............. render (source of) light cube ................
        light_shader.use();
        // view/projection transformations
        light_shader._setLocMat4("Project",project)._setLocMat4("View",view);
        
        glBindVertexArray(light_VAO);
        glDrawArrays(GL_TRIANGLES, 0, VERTEX_COUNT);
        
        // glBindVertexArray(0); // no need to unbind it every time
        glfwSwapBuffers(window);
        glfwPollEvents();
        
        // ......... debug ...........................
        if (do_debug) {
            prev_param = param;
            prev_p_y_f = p_y_f;
        }
        // ....................................
    }
    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &light_VAO);
    glDeleteVertexArrays(1, &cube_VAO);
    glDeleteBuffers(1, &VBO);
  
    
    glfwTerminate();
    return 0;
  }
  catch (const std::string& e) {
    std::cerr << PRG << e << std::endl;
  }
  catch (const std::system_error& e) {
    std::cerr << PRG << e.what() << " (" << e.code() << ")" << std::endl;
  }
  catch (...) {
    std::cerr << PRG << " caught an unknown exception" << std::endl;
  }
    glfwTerminate();
    return -1;
}
