#include "process.h"

std::tuple<unsigned int,unsigned int, unsigned int, unsigned int>
prepVertices ()
{
    struct Vertex3
    {
      Vertex3(glm::vec3 Pos, glm::vec3 Color, glm::vec2 TexCoord) {
         position_ = Pos;
         color_    = Color;
         texCoord_ = TexCoord;
      }
        
      glm::vec3 position_;
      glm::vec3 color_;
      glm::vec2 texCoord_;
    };
// ------------------------------------------------------------------
  float vertices[] = {
    // positions          // colors           // texture coords
    0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // top right
    0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // bottom right
    -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // bottom left
    -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // top left 
  };
  unsigned int indices[] = {
    0, 1, 3, // first triangle
    1, 2, 3  // second triangle
  };

  Vertex3 ver_1(glm::vec3( 0.5f,  0.5f, 0.0f), glm::vec3( 1.0f, 0.0f, 0.0f), glm::vec2( 1.0f, 1.0f));
  Vertex3 ver_2(glm::vec3( 0.5f, -0.5f, 0.0f), glm::vec3( 0.0f, 1.0f, 0.0f), glm::vec2(1.0f, 0.0f));   // bottom right
  Vertex3 ver_3(glm::vec3( -0.5f, -0.5f, 0.0f), glm::vec3( 0.0f, 0.0f, 1.0f), glm::vec2(0.0f, 0.0f));   // bottom left
  Vertex3 ver_4(glm::vec3( -0.5f,  0.5f, 0.0f), glm::vec3( 1.0f, 1.0f, 0.0f), glm::vec2(0.0f, 1.0f));    // top left 

  
  unsigned int VBO, VAO, EBO;
  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);
  glGenBuffers(1, &EBO);
  // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
  glBindVertexArray(VAO);
  
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
  // __________________ setup offsets into vertex attributes _________________
  //   vertexCoords: 0:position,3:size, and 6: 2 coords
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  //   color: 1:position,3:size, and 8: 2 coords
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3*sizeof(float)));
  glEnableVertexAttribArray(1);
  //   texCoords: 2:position,2:size, and 6: 2 coords
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6*sizeof(float)));
  glEnableVertexAttribArray(2);
  
    // 0: <- vertex count
  return std::tuple<unsigned int,unsigned int, unsigned int, unsigned int>(6,VBO,VAO,EBO);
}  
 
