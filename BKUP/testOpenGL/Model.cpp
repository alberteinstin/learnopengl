#include <iostream>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Model.h" 

using namespace std;
using namespace mesh;

void
Model::load_model_(const std::string& path)
{
    const char* const PRG = "Model::load_model_> ";
  directory_ = "";
  Assimp::Importer importer;
  const aiScene *scene = importer.ReadFile(path.c_str(), aiProcess_Triangulate | aiProcess_FlipUVs);

      if(!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) 
    {
        stringstream os;
        os << PRG << "ERROR::ASSIMP:: "<< importer.GetErrorString() << ", for path: " << path << endl;
        throw os.str();
     }
    directory_ = path.substr(0, path.find_last_of('/'));

    process_node_(scene->mRootNode, scene);

}

void
Model::process_node_(aiNode *node, const aiScene *scene)
{
    // process all the node's meshes (if any)
  for(unsigned int i = 0; i < node->mNumMeshes; i++)
  {
    aiMesh *mesh = scene->mMeshes[node->mMeshes[i]]; 
    meshes_.push_back(process_mesh_(mesh, scene));
  }
  // then do the same for each of its children
  for(unsigned int i = 0; i < node->mNumChildren; i++)
  {
    process_node_(node->mChildren[i], scene);
  }
}  

Mesh
Model::process_mesh_(aiMesh *mesh, const aiScene *scene)
{
    const char* const PRG = "Model::process_mesh_> ";
  vector<mesh::Vertex> vertices;
  vector<unsigned int> indices;
  vector<Texture> textures;
  
  // process vertex positions, normals and texture coordinates
  for(unsigned int i = 0; i < mesh->mNumVertices; i++)
  {
    Vertex vertex;
     const auto& mesh_vertex = mesh->mVertices[i];
     vertex.position = glm::vec3(mesh_vertex.x, mesh_vertex.y, mesh_vertex.z);

    if (mesh->HasNormals())
    {
       const auto& mesh_normals = mesh->mNormals[i];
        vertex.normal = glm::vec3(mesh_normals.x, mesh_normals.y, mesh_normals.z);
    }
    if(mesh->mTextureCoords[0]) // does the mesh contain texture coordinates?
    {
       const auto& mesh_tex_coord = mesh->mTextureCoords[0][i];
        vertex.texCoords = glm::vec2(mesh_tex_coord.x, mesh_tex_coord.y);
      
      if(mesh->mTangents)
      {
        const auto& mesh_tangent = mesh->mTangents[i];
         vertex.tangent = glm::vec3(mesh_tangent.x, mesh_tangent.y, mesh_tangent.z);
      }
      
      if(mesh->mBitangents)
      {
        const auto& mesh_bitangent = mesh->mBitangents[i];
          vertex.bitangent = glm::vec3(mesh_bitangent.x, mesh_bitangent.y, mesh_bitangent.z);
      }
    }
    else
      vertex.texCoords = glm::vec2(0.0f, 0.0f);
 
    vertices.push_back(vertex);
  }
  // now walk through each of the mesh's faces (a face in a mesh is its triangle) and retrieve the corresponding vertex indices.
  for(unsigned int i = 0; i < mesh->mNumFaces; i++)
  {
    aiFace face = mesh->mFaces[i];
    // retrieve all indices of the face and store them in the indices vector
    for(unsigned int j = 0; j < face.mNumIndices; j++)
      indices.push_back(face.mIndices[j]);        
  }
  // process materials
  aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
    
  // we assume a convention for sampler names in the shaders. Each diffuse texture should be named
  // as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER. 
  // Same applies to other texture as the following list summarizes:
  // eg: diffuse: texture_diffuseN, specular: texture_specularN, normal: texture_normalN
  
  // 1. diffuse maps
    vector<Texture> diffuseMaps = load_material_textures_(material, aiTextureType_DIFFUSE, Texture::eType::eDiffuse);
  textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
   // cout << PRG << "# of diffuse textures: " << diffuseMaps.size() << endl;
  // 2. specular maps
  vector<Texture> specularMaps = load_material_textures_(material, aiTextureType_SPECULAR, Texture::eType::eSpecular);
 // cout << PRG << "# of specular textures: " << specularMaps.size() << endl;
  textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
  // 3. normal maps
  std::vector<Texture> normalMaps = load_material_textures_(material, aiTextureType_NORMALS, Texture::eType::eNormal);
 // cout << PRG << "# of normal textures: " << normalMaps.size() << endl;
  textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());
        // 4. ambient maps
   std::vector<Texture> ambientMaps = load_material_textures_(material, aiTextureType_AMBIENT, Texture::eType::eHeight);
//  cout << PRG << "# of ambient textures: " << ambientMaps.size() << endl;
    textures.insert(textures.end(), ambientMaps.begin(), ambientMaps.end());
        // 4. height maps
  std::vector<Texture> heightMaps = load_material_textures_(material, aiTextureType_HEIGHT, Texture::eType::eHeight);
 // cout << PRG << "# of height textures: " << heightMaps.size() << endl;
    textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());

  // return a mesh object created from the extracted mesh data
  return Mesh(move(vertices), move(indices), move(textures));
  
}  

std::vector<Texture>
Model::load_material_textures_(aiMaterial *mat, aiTextureType type, Texture::eType texType)
{
  const char* const PRG = "Model::load_material_textures_> ";
  
  vector<Texture> textures;
  try {
    
  for(unsigned int i = 0; i < mat->GetTextureCount(type); i++)
  {
    aiString str;
    mat->GetTexture(type, i, &str);
  //  Texture texture;
    // check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
    bool skip = false;
    for(unsigned int j = 0; j < textures_loaded_.size(); j++)
    {
      if(std::strcmp(textures_loaded_[j].Path().data(), str.C_Str()) == 0)
      {
        textures.push_back(textures_loaded_[j]);
        skip = true; // a texture with the same filepath has already been loaded, continue to next one. (optimization)
        break;
      }
    }
    if(!skip)
    {   // if texture hasn't been loaded already, load it
       string tex_name = str.C_Str();
       Texture texture(tex_name, directory_,texType);  // may throw
       textures.push_back(texture);
       textures_loaded_.push_back(texture);  // no unnecesery load duplicate textures.
    }
  }
  }
    catch (const std::system_error& e) {
    std::cerr << PRG << e.what() << " (" << e.code() << ")" << std::endl;
    throw e;
  }

  return move(textures);
}  
