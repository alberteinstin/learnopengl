#include <vector>

#include "util.h"  // clamp
#include "process.h"


extern float g_deltaTime;// Time between current frame and last frame
extern float g_lastFrame;// Time of last frame
extern float g_lastX;
extern float g_lastY;
extern Camera g_cam;


void processInput (GLFWwindow* pWindow, Camera& Cam)
{
  const char *const PRG = "processInput> ";

  using namespace std;
    
   
    if (glfwGetKey(pWindow, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(pWindow, true);

   //  static unsigned char wireframe = 0;
    // if (glfwGetKey(pWindow, GLFW_KEY_W) == GLFW_PRESS)
    //   glPolygonMode(GL_FRONT_AND_BACK, (wireframe = 1 - wireframe) ? GL_LINE : GL_FILL);

    const float cameraSpeed = 2.5f * g_deltaTime;            // = 0.05f; // adjust accordingly
    if (glfwGetKey(pWindow, GLFW_KEY_W) == GLFW_PRESS) {
      Cam._zoom(cameraSpeed);
 //       cout << PRG << cameraPos << endl;
    }
    if (glfwGetKey(pWindow, GLFW_KEY_S) == GLFW_PRESS) {
      Cam._zoom(-cameraSpeed);
   //     cout << PRG << cameraPos << endl;
    }
    if (glfwGetKey(pWindow, GLFW_KEY_A) == GLFW_PRESS) {
      Cam._strafe(-cameraSpeed);
     //   cout << PRG << cameraPos << endl;
    }
    if (glfwGetKey(pWindow, GLFW_KEY_D) == GLFW_PRESS) {
      Cam._strafe(cameraSpeed);
    }

}

void processInput (GLFWwindow* pWindow)
{
    if (glfwGetKey(pWindow, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(pWindow, true);

    static unsigned char wireframe = 0;

    if (glfwGetKey(pWindow, GLFW_KEY_W) == GLFW_PRESS)
      glPolygonMode(GL_FRONT_AND_BACK, (wireframe = 1 - wireframe) ? GL_LINE : GL_FILL);

}


GLFWwindow* setupWindow (int Width, int Height, const char* Name)
{
  using namespace std;
  glfwInit();
  
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,3);
  glfwWindowHint(GLFW_OPENGL_PROFILE,GLFW_OPENGL_CORE_PROFILE);
  
#ifdef __APPLE__
  // uncomment this statement to fix compilation on OS X
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
  
  GLFWwindow* p_window = glfwCreateWindow(Width, Height, Name, NULL, NULL);
  if (!p_window) {
    std::cout << "Failed to create GLFW window" << std::endl;
//      glfwTerminate();
    throw "Failed to create GLFW window";
  }
  glfwMakeContextCurrent(p_window);
  // this call connect opengl driver particular to this machine
  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
    cout << "Failed to initialize GLAD" << endl;
    throw "Failed to initialize GLAD";
  }
  glViewport(0,0,Width,Height);  // size of gl rendering window
    // mouse cursor stays within the center of the window
    glfwSetInputMode(p_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    
  // ... register callbacks ......
  glfwSetFramebufferSizeCallback(p_window, framebuffer_size_callback); // register
  glfwSetCursorPosCallback(p_window, mouse_callback);
  glfwSetScrollCallback(p_window, scroll_callback);
  
  return p_window;
}

void framebuffer_size_callback(GLFWwindow* p_window, int w, int h)
{
    glViewport(0,0,w,h);
}
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
    g_cam._updateFov(-yoffset,true);
    
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    static bool firstMouse = true;
    if (firstMouse)
    {
        g_lastX = xpos;
        g_lastY = ypos;
        firstMouse = false;
    }
  
    float xoffset = xpos - g_lastX;
    float yoffset = g_lastY - ypos; 
    g_lastX = xpos;
    g_lastY = ypos;

    float sensitivity = 0.1f;
    xoffset *= sensitivity;
    yoffset *= sensitivity;

    g_cam._updateYaw(xoffset)._updatePitch(yoffset,true);
 
 }  
