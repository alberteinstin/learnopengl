#ifndef MODEL_H_
#define MODEL_H_

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Mesh.h" 


class Model 
{
public:
  Model(const std::string& path): gamma_correction_{1}
    {
      load_model_(path);
    }
  void draw(Shader &shader) {
    for(unsigned int i = 0; i < meshes_.size(); i++)
        meshes_[i].draw(shader);
  }  	
private:
  // model data
  std::vector<Texture> textures_loaded_;
  std::vector<mesh::Mesh> meshes_;
  std::string directory_;
  bool gamma_correction_;
  
  void load_model_(const std::string& path);
  
  void process_node_(aiNode *node, const aiScene *scene);
  mesh::Mesh process_mesh_(aiMesh *mesh, const aiScene *scene);

  std::vector<Texture> load_material_textures_(aiMaterial *mat, aiTextureType type, Texture::eType texType);
 
};

#endif
