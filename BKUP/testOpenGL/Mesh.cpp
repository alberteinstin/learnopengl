
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Mesh.h"

using namespace mesh;
using namespace std;

void
Mesh::draw(const Shader& shader) const 
{
  // bind appropriate textures
  unsigned int diffuseNr  = 0, specularNr = 0;
  unsigned int normalNr   = 0, heightNr   = 0, ambientNr = 0;
  
  for(unsigned int i = 0; i < textures_.size(); i++)
  {
    glActiveTexture(GL_TEXTURE0 + i); // active proper texture unit before binding
    // retrieve texture number (the N in diffuse_textureN)
    string number;
      const auto type = textures_[i].Type();
      if (type == Texture::eType::eDiffuse)
      number = std::to_string(diffuseNr++);
    else if(type == Texture::eType::eSpecular)
      number = std::to_string(specularNr++); // transfer unsigned int to stream
    else if(type == Texture::eType::eNormal)
        number = std::to_string(normalNr++); // transfer unsigned int to stream
    else if(type == Texture::eType::eHeight)
        number = std::to_string(heightNr++);
      else if(type == Texture::eType::eAmbient)
          number = std::to_string(ambientNr++);

      std::string tex_name = Texture::TypeName[int(type)]+ number;  // <== convention for texture name, e.g. "texture_diffuse1"
      shader._setUniformLocInt(tex_name, i); // set the sampler to the correct texture unit
  
    // and finally bind the texture
    glBindTexture(GL_TEXTURE_2D, textures_[i].ID());
  }
  
  // draw mesh
    glBindVertexArray(VAO_);
  glDrawElements(GL_TRIANGLES, indices_.size(), GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);
  
  // always good practice to set everything back to defaults once configured.
  glActiveTexture(GL_TEXTURE0);
}

void
Mesh::setup_mesh_() const
{
  // create buffers/arrays
  glGenVertexArrays(1, &VAO_);
  glGenBuffers(1, &VBO_);
  glGenBuffers(1, &EBO_);
  
  glBindVertexArray(VAO_);
  // load data into vertex buffers
  glBindBuffer(GL_ARRAY_BUFFER, VBO_);
  // A great thing about structs is that their memory layout is sequential for all its items.
  // The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
  // again translates to 3/2 floats which translates to a byte array.
  glBufferData(GL_ARRAY_BUFFER, vertices_.size() * sizeof(Vertex), &vertices_[0], GL_STATIC_DRAW);  

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO_);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_.size() * sizeof(unsigned int), &indices_[0], GL_STATIC_DRAW);

  // set the vertex attribute pointers
  // vertex Positions
  glEnableVertexAttribArray(0);	
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
  // vertex normals
  glEnableVertexAttribArray(1);	
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
  // vertex texture coords
  glEnableVertexAttribArray(2);	
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoords));
        // vertex tangent
  glEnableVertexAttribArray(3);
  glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, tangent));
  // vertex bitangent
  glEnableVertexAttribArray(4);
  glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, bitangent));
  
  glBindVertexArray(0);
}
