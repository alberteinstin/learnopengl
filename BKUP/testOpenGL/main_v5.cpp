//
//  OpenGL
//
//  Created by Albert Einstin on 11/18/19.
//  Copyright © 2019 Albert Einstin. All rights reserved.
//

#include <iostream>
#include <vector>
#include "glad/glad.h"
#include "GLFW/glfw3.h"


#include "Shader.h"
#include "Texture.h"   // <-  #include "stb_image.h"
#include "Camera.h"

#include "process.h"   // <- processInput,  setupWindow

const unsigned int SCR_WIDTH = 800, SCR_HEIGHT = 600;   //  view port size

float g_deltaTime = 0.f;// Time between current frame and last frame
float g_lastFrame = 0.f;// Time of last frame
float g_lastX = SCR_WIDTH/2.0f;
float g_lastY = SCR_HEIGHT/2.f;

Camera    g_cam(glm::vec3(0.0f, 0.0f, 3.0f));  // position
 
int main(int argc, const char * argv[])
{
    const char* const PRG = "main> ";

  using namespace std;

     // ................ positions (world space) of cube clones
       glm::vec3 cubePositions[] = {
         glm::vec3( 0.0f,  0.0f,  0.0f),
         glm::vec3( 2.0f,  5.0f, -15.0f),
         glm::vec3(-1.5f, -2.2f, -2.5f),
         glm::vec3(-3.8f, -2.0f, -12.3f),
         glm::vec3( 2.4f, -0.4f, -3.5f),
         glm::vec3(-1.7f,  3.0f, -7.5f),
         glm::vec3( 1.3f, -2.0f, -2.5f),
         glm::vec3( 1.5f,  2.0f, -2.5f),
         glm::vec3( 1.5f,  0.2f, -1.5f),
         glm::vec3(-1.3f,  1.0f, -1.5f)
       };
       const auto POSITION_COUNT = sizeof(cubePositions)/sizeof(glm::vec3);
    
  try {
    GLFWwindow* window = setupWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL");
    
// ........... shader processing (compile+link+program on ctor)
    Shader shader(loc_home()+"shaders/shader_v4.vs","shader_v2.fs");
  // ------------------------------------------------------------------
    auto buf_id = prepVertices(); // 
    const auto VERTEX_COUNT = get<0>(buf_id);
    const auto VBO = get<1>(buf_id), VAO = get<2>(buf_id), EBO = get<3>(buf_id);
   
   // ................. generating/binding Textures
    Texture tex_0(loc_home()+"textures/wooden_container.jpg");
    auto texture_id_0 = tex_0.ID();
    Texture tex_1(loc_home()+"textures/awesomeface.png",true); // <- T: flip image
    auto texture_id_1 = tex_1.ID();
     
    // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
    // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
    glBindVertexArray(0);
    
      const bool do_camera= true, do_swing_camera_around = false;
    // tell opengl for each sampler to which texture unit it belongs to (only has to be done once)
      shader.use(); // activate/use the shader before setting uniforms!
      shader._setUniformLocInt("Texture_0", 0)._setUniformLocInt("Texture_1", 1);
      // ................. camera setup
      glm::mat4 project = glm::mat4(1.0f), view = g_cam.lookAt();
      
      glEnable(GL_DEPTH_TEST);
      
      // transformations
      auto fov = do_camera ? g_cam.FovInRad() : glm::radians(45.0f);
      view = do_camera ? g_cam.lookAt() : glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -3.0f));
      project = glm::perspective (fov, (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
      
      shader._setUniformLocMat4("View",view)._setUniformLocMat4("Project",project);
      
      unsigned int iter = 0;
    // ------------  render loop -------------
    while (!glfwWindowShouldClose(window)) {
      
      float currentFrame = glfwGetTime();
      g_deltaTime = currentFrame - g_lastFrame;
        g_lastFrame = currentFrame;
        if (do_camera) {
            processInput(window, g_cam); // input from keyboard/mouse
            if (do_swing_camera_around) {
                const float radius = 10.f;
                g_cam.Pos() = glm::vec3(sin(glfwGetTime()) * radius, 0.0f, cos(glfwGetTime())* radius);
            }
            view = g_cam.lookAt();
            shader._setUniformLocMat4("View", view);
            
        }
        else
            processInput(window);
        // render
      glClearColor(0.2f,0.3f,0.3f,1.0f);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear z-buffer
      // activate the texture unit first before binding texture
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, texture_id_0);
      glActiveTexture(GL_TEXTURE1);
      glBindTexture(GL_TEXTURE_2D, texture_id_1);

 
   //     if (iter%1000)
  //      cout <<  "{" << iter << "} " << camera_Pos << "; " << camera_Front << "; " << camera_Up << endl;
      
      glBindVertexArray(VAO); // seeing as we only have a single VAO there's no need to bind it every time;

      for (unsigned int i = 0; i < POSITION_COUNT; i++)
      {
        glm::mat4 model = glm::mat4(1.0f);
        model = glm::translate(model,cubePositions[i]);
        auto angle_rad = glm::radians(20.f *i);
        if ((i%3) == 0)
           angle_rad *= (float)glfwGetTime();
        model = glm::rotate(model, angle_rad, glm::vec3(1.0f, 0.3f, 0.5f)); // map to world space
        // ...........render container .......................
          shader._setUniformLocMat4("Model",model);
 
        glDrawArrays(GL_TRIANGLES, 0, VERTEX_COUNT);
      }
      // glBindVertexArray(0); // no need to unbind it every time
      glfwSwapBuffers(window);
      glfwPollEvents();
      iter++;
      // if (iter > 5000) break;
    }
    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
      if (EBO)
        glDeleteBuffers(1, &EBO);
    
    
    glfwTerminate();
    return 0;
  }
  catch (const std::string& e) {
    std::cerr << PRG << e << std::endl;
  }
  catch (const std::system_error& e) {
    std::cerr << PRG << e.what() << " (" << e.code() << ")" << std::endl;
  }
  catch (...) {
    std::cerr << PRG << " caught an unknown exception" << std::endl;
  }
    
  glfwTerminate();
  return -1;
  
}
