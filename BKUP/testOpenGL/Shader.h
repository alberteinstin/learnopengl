//
 //  OpenGL
//
//  Created by Albert Einstin on 11/18/19.
//  Copyright © 2019 Albert Einstin. All rights reserved.
//

#ifndef SHADER_H
#define SHADER_H

#include <glad/glad.h> // include glad to get all the required OpenGL headers
  
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
  
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Shader
{
    typedef Shader SELF;
public:
  
  // constructor reads and builds the shader
    Shader(const std::string& vertexPath, std::string fragmentPath, const std::string& geometryPath = "");
  // use/activate this shader
  void use() { glUseProgram(ID_); }
    
  unsigned int ID () const { return ID_; }
    
  // utility uniform functions
  const SELF& _setUniformLocBool(const std::string &name, bool value) const {
    glUniform1i(glGetUniformLocation(ID_, name.c_str()), (int)value);
      return *this;
  }
  //alias
  const SELF& _setLocBool(const std::string &name, bool value) const { return _setUniformLocBool(name,value); }
  
  const SELF& _setUniformLocInt(const std::string &name, int value) const {
    glUniform1i(glGetUniformLocation(ID_, name.c_str()), value);
      return *this;
  }
    
  const SELF&  _setUniformLocFloat(const std::string &name, float value) const {
     glUniform1f(glGetUniformLocation(ID_, name.c_str()), value);
      return *this;
  }

  const SELF& _setUniformLocVec2(const std::string &name, const glm::vec2 &value) const
    { 
        glUniform2fv(glGetUniformLocation(ID_, name.c_str()), 1, &value[0]); 
    return *this; }
    const SELF& _setUniformLocVec2(const std::string &name, float x, float y) const
    { 
        glUniform2f(glGetUniformLocation(ID_, name.c_str()), x, y); 
    return *this; }
    // ------------------------------------------------------------------------
    const SELF& _setUniformLocVec3(const std::string &name, const glm::vec3 &value) const
    { 
        glUniform3fv(glGetUniformLocation(ID_, name.c_str()), 1, &value[0]); 
    return *this; }
    const SELF& _setUniformLocVec3(const std::string &name, float x, float y, float z) const
    { 
        glUniform3f(glGetUniformLocation(ID_, name.c_str()), x, y, z); 
    return *this; }
    // ------------------------------------------------------------------------
    const SELF& _setUniformLocVec4(const std::string &name, const glm::vec4 &value) const
    { 
        glUniform4fv(glGetUniformLocation(ID_, name.c_str()), 1, &value[0]); 
    return *this; }
    const SELF& _setUniformLocVec4(const std::string &name, float x, float y, float z, float w) 
    { 
        glUniform4f(glGetUniformLocation(ID_, name.c_str()), x, y, z, w); 
    return *this; }
    // ------------------------------------------------------------------------
    const SELF& _setUniformLocMat2(const std::string &name, const glm::mat2 &mat) const
    {
        glUniformMatrix2fv(glGetUniformLocation(ID_, name.c_str()), 1, GL_FALSE, &mat[0][0]);
    return *this; }
    // ------------------------------------------------------------------------
    const SELF& _setUniformLocMat3(const std::string &name, const glm::mat3 &mat) const
    {
        glUniformMatrix3fv(glGetUniformLocation(ID_, name.c_str()), 1, GL_FALSE, &mat[0][0]);
    return *this; }

  const SELF& _setUniformLocMat4(const std::string &name, const glm::mat4& value) const {
    glUniformMatrix4fv(glGetUniformLocation(ID_, name.c_str()), 1, GL_FALSE, glm::value_ptr(value));
      return *this;
  }
  
  private:
  GLint check_compile_errors_(GLuint shader, std::string type);
  
private:
    unsigned int ID_; // the program ID
};
  
#endif
