#ifndef process_H
#define process_H

#include <iostream>
#include <vector>
#include <tuple>

#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Camera.h" 

inline auto loc_home() {
    return std::string("/Users/alb/Albert/xcode/testOpenGL/");
}
// .......declarations
void framebuffer_size_callback(GLFWwindow*, int w, int h);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);


void processInput (GLFWwindow* pWindow);
void processInput (GLFWwindow* pWindow, Camera& Cam);

GLFWwindow* setupWindow (int Width, int Hight, const char* Name = "No Name");

//  vertex_count, VBO, VAO, EBO;
std::tuple<unsigned int,unsigned int,unsigned int, unsigned int> prepVertices ();
// two boxes
std::vector<std::tuple<unsigned int,unsigned int,unsigned int>> prepLightVertices ();


inline std::ostream& operator << (std::ostream& os, const glm::vec3& rhs) {
  os << "[" << rhs[0] << "," << rhs[1] << "," << rhs[2] << "]";
    return os;
}

#endif
