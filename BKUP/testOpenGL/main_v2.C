//
//  OpenGL
//
//  Created by Albert Einstin on 11/18/19.
//  Copyright © 2019 Albert Einstin. All rights reserved.
//

#include <iostream>

#include "glad/glad.h"
#include "GLFW/glfw3.h"


#include "Shader.h"
#include "Texture.h"   // <-  #include "stb_image.h"
#include "process.h"   // <- processInput,  setupWindow

int main(int argc, const char * argv[])
{
  using namespace std;
// settings
  const unsigned int SCR_WIDTH = 800;
  const unsigned int SCR_HEIGHT = 600;

  try {
      GLFWwindow* window = setupWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL");
    
// ........... shader processing (compile+link+program on ctor)
    Shader shader("/Users/alb/opengl/OpenGL/shaders/shader_v2.vs","/Users/alb/opengl/OpenGL/shaders/shader_v2.fs");
  
  // ------------------------------------------------------------------
    auto buf_id = prepVertices(); // 
   
   auto VBO = get<0>(buf_id), VAO = get<1>(buf_id), EBO = get<2>(buf_id);
   
   // ................. generating/binding Textures
    Texture tex_0("/Users/alb/opengl/OpenGL/textures/wooden_container.jpg");
    auto texture_id_0 = tex_0.ID();
    Texture tex_1("/Users/alb/opengl/OpenGL/textures/awesomeface.png",true); // <- T: flip image
    auto texture_id_1 = tex_1.ID();
    
    
    // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
    // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
    glBindVertexArray(0);
    
    // tell opengl for each sampler to which texture unit it belongs to (only has to be done once)
    shader.use(); // activate/use the shader before setting uniforms!
    shader.setUniformLocInt("Texture_0", 0); // `Texture_0' is `uniform' sampler2D in fragment shader
    shader.setUniformLocInt("Texture_1", 1);
    
    // ------------  render loop -------------
    while (!glfwWindowShouldClose(window)) {
      // input
      processInput(window);
      // render
      glClearColor(0.2f,0.3f,0.3f,1.0f);
      glClear(GL_COLOR_BUFFER_BIT);
      // activate the texture unit first before binding texture
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, texture_id_0);
      glActiveTexture(GL_TEXTURE1);
      glBindTexture(GL_TEXTURE_2D, texture_id_1);
      
//      create transformations; init all to identity
      glm::mat4 model = glm::mat4(1.0f), view = model, project = model;
      model = glm::rotate(model, glm::radians(-55.0f), glm::vec3(1.0f, 0.0f, 0.0f)); // map to world space
      view = glm::translate(view, glm::vec3(0.f, 0.f, -3.0f));
        project = glm::perspective(glm::radians(45.0f),float(SCR_WIDTH)/float(SCR_HEIGHT),0.1f,100.f);
      
 
      // ...........Render container .......................
      shader.use();
      shader.setUniformLocMtx4("Model",model);
       shader.setUniformLocMtx4("View",view);
      shader.setUniformLocMtx4("Project",project);
      
      glBindVertexArray(VAO); // seeing as we only have a single VAO there's no need to bind it every time, but we'll do so to keep things a bit more organized
       glDrawElements(GL_TRIANGLES,6,GL_UNSIGNED_INT,0);
//        glDrawArrays(GL_TRIANGLES, 0, 3);
      // glBindVertexArray(0); // no need to unbind it every time
      
      glfwSwapBuffers(window);
      glfwPollEvents();
    }
    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
    
    
    glfwTerminate();
    return 0;
  }
  catch (...) {
    glfwTerminate();
    return -1;
  }
}
