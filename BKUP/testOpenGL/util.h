#ifndef util_H
#define util_H

template <typename T> T clamp(const T& V, const T& low, const T& up) {
  return V > up ? up : ( V < low ? low : V);
}

#endif
