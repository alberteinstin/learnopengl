//
//  OpenGL
//
//  Created by Albert Einstin on 11/18/19.
//  Copyright © 2019 Albert Einstin. All rights reserved.
//

#include <iostream>
#include <vector>
#include "glad/glad.h"
#include "GLFW/glfw3.h"


#include "Shader.h"
#include "Texture.h"   // <-  #include "stb_image.h"
#include "process.h"   // <- processInput,  setupWindow

float g_deltaTime = 0.f;// Time between current frame and last frame
float g_lastFrame = 0.f;// Time of last frame
double g_lastX = 0.f;
double g_lastY = 0.f;
float g_pitch = 0.f;
float g_yaw   = -90.f; // deg
float g_fov = 45.f;

glm::vec3 g_cameraFront;

int main(int argc, const char * argv[])
{
  using namespace std;

  const unsigned int SCR_WIDTH = 800, SCR_HEIGHT = 600;   //  view port size

  try {
      GLFWwindow* window = setupWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL");
    
// ........... shader processing (compile+link+program on ctor)
    Shader shader(loc_home()+"shaders/shader_v4.vs","shader_v2.fs");
  
  // ------------------------------------------------------------------
    auto buf_id = prepVertices(); // 
    const auto VERTEX_COUNT = get<0>(buf_id);
    const auto VBO = get<1>(buf_id), VAO = get<2>(buf_id), EBO = get<3>(buf_id);
   
   // ................. generating/binding Textures
    Texture tex_0(loc_home()+"textures/wooden_container.jpg");
    auto texture_id_0 = tex_0.ID();
    Texture tex_1(loc_home()+"textures/awesomeface.png",true); // <- T: flip image
    auto texture_id_1 = tex_1.ID();
    
     // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
    // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
    glBindVertexArray(0);
    
    // tell opengl for each sampler to which texture unit it belongs to (only has to be done once)
    shader.use(); // activate/use the shader before setting uniforms!
    shader.setUniformLocInt("Texture_0", 0); // `Texture_0' is `uniform' sampler2D in fragment shader
    shader.setUniformLocInt("Texture_1", 1);
    // ................. camera setup
    /*
    glm::vec3 cameraPos = glm::vec3(0.f,0.f,3.f), cameraTarget = glm::vec3(0.f,0.f,0.f);
    glm::vec3 cameraDir = glm::normalize(cameraPos - cameraTarget);  // point towards camera
    // define camera-centric coords
    glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);  // assume camera is straight up
    glm::vec3 cameraRight = glm::normalize(glm::cross(up, cameraDir));
    glm::vec3 cameraUp =  glm::normalize(glm::cross(cameraDirection, cameraRight));

    glm::mat4 view = glm::lookAt(glm::vec3(0.0f, 0.0f, 3.0f),  // camera position
                                 glm::vec3(0.0f, 0.0f, 0.0f),   // camera target
                                 glm::vec3(0.0f, 1.0f, 0.0f));  // camera up
    */
    std::vector<glm::vec3*> camera_vec;
    glm::vec3 cameraPos   = glm::vec3(0.0f, 0.0f,  3.0f); camera_vec.push_back(&cameraPos);
    g_cameraFront         = glm::vec3(0.0f, 0.0f, -1.0f); camera_vec.push_back(&g_cameraFront);
    glm::vec3 cameraUp    = glm::vec3(0.0f, 1.0f,  0.0f); camera_vec.push_back(&cameraUp);
    
    glm::mat4 view = glm::lookAt(cameraPos, cameraPos + g_cameraFront, cameraUp);
    // ................ positions (world space) of cube clones
    glm::vec3 cubePositions[] = {
      glm::vec3( 0.0f,  0.0f,  0.0f), 
      glm::vec3( 2.0f,  5.0f, -15.0f), 
      glm::vec3(-1.5f, -2.2f, -2.5f),  
      glm::vec3(-3.8f, -2.0f, -12.3f),  
      glm::vec3( 2.4f, -0.4f, -3.5f),  
      glm::vec3(-1.7f,  3.0f, -7.5f),  
      glm::vec3( 1.3f, -2.0f, -2.5f),  
      glm::vec3( 1.5f,  2.0f, -2.5f), 
      glm::vec3( 1.5f,  0.2f, -1.5f), 
      glm::vec3(-1.3f,  1.0f, -1.5f)  
    };
    const auto POSITION_COUNT = sizeof(cubePositions)/sizeof(glm::vec3);
    
    glEnable(GL_DEPTH_TEST);
    
    // transformations
    glm::mat4 project = glm::mat4(1.f);
    project = glm::perspective (glm::radians(g_fov), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
    shader.setUniformLocMtx4("Project",project);// pass to shader

    unsigned int iter = 0;
    // ------------  render loop -------------
    while (!glfwWindowShouldClose(window)) {
      
      float currentFrame = glfwGetTime();
      g_deltaTime = currentFrame - g_lastFrame;
      g_lastFrame = currentFrame;
      
     processInput(window, camera_vec); // input from keyboard/mouse

      const auto & camera_Pos   = *camera_vec[0];
      const auto& camera_Front = *camera_vec[1];
      const auto& camera_Up    = *camera_vec[2];

      
      // render
      glClearColor(0.2f,0.3f,0.3f,1.0f);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear z-buffer
      // activate the texture unit first before binding texture
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, texture_id_0);
      glActiveTexture(GL_TEXTURE1);
      glBindTexture(GL_TEXTURE_2D, texture_id_1);

      shader.use();
      
//      create transformations
      view = glm::lookAt(camera_Pos, camera_Pos + camera_Front, camera_Up);
      shader.setUniformLocMtx4("View",view);
        
 //     if (iter%1000)
  //      cout <<  "{" << iter << "} " << camera_Pos << "; " << camera_Front << "; " << camera_Up << endl;
      
      glBindVertexArray(VAO); // seeing as we only have a single VAO there's no need to bind it every time;

      for (unsigned int i = 0; i < POSITION_COUNT; i++)
      {
        
        glm::mat4 model = glm::mat4(1.0f);
        model = glm::translate(model,cubePositions[i]);
        auto angle_rad = glm::radians(20.f *i);
        model = glm::rotate(model, angle_rad, glm::vec3(1.0f, 0.3f, 0.5f)); // map to world space
        // ...........render container .......................
        shader._setUniformLocMtx4("Model",model);
        
        glDrawArrays(GL_TRIANGLES, 0, VERTEX_COUNT);
      }
      // glBindVertexArray(0); // no need to unbind it every time
      glfwSwapBuffers(window);
      glfwPollEvents();
      iter++;
      // if (iter > 5000) break;
    }
    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
      if (EBO)
        glDeleteBuffers(1, &EBO);
    
    
    glfwTerminate();
    return 0;
  }
  catch (...) {
    glfwTerminate();
    return -1;
  }
}
