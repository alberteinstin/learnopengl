//
//  OpenGL
//
//  Created by Albert Einstin on 11/18/19.
//  Copyright © 2019 Albert Einstin. All rights reserved.
//

#include <iostream>

#include "glad/glad.h"
#include "GLFW/glfw3.h"


#include "Shader.h"
#include "Texture.h"   // <-  #include "stb_image.h"
#include "process.h"   // <- processInput,  setupWindow

float g_deltaTime = 0.f;// Time between current frame and last frame
float g_lastFrame = 0.f;// Time of last frame
float g_lastX = 0.f;
float g_lastY = 0.f;

Camera g_cam(glm::vec3(0.0f, 0.0f,  3.0f));  // init position in world

int main(int argc, const char * argv[])
{
    const char* const PRG = "main> ";
  using namespace std;

  const unsigned int SCR_WIDTH = 800;
  const unsigned int SCR_HEIGHT = 600;

  try {
    GLFWwindow* window = setupWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL");
    
// ........... shader processing (compile+link+program on ctor)
    Shader shader(loc_home() + "shaders/shader_v1.vs","shader_v1.fs");
  
  // ------------------------------------------------------------------
    auto buf_id = prepVertices(); // 
   
   auto VBO = get<0>(buf_id), VAO = get<1>(buf_id), EBO = get<2>(buf_id);
   
   // ................. generating/binding Textures
    Texture tex_0(loc_home() + "textures/wooden_container.jpg");
    auto texture_id_0 = tex_0.ID();
    Texture tex_1(loc_home() + "textures/awesomeface.png",true); // <- T: flip image
    auto texture_id_1 = tex_1.ID();
    
    
    // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
    // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
    glBindVertexArray(0);
    
    // tell opengl for each sampler to which texture unit it belongs to (only has to be done once)
    shader.use(); // activate/use the shader before setting uniforms!
      shader._setUniformLocInt("Texture_0", 0); // `Texture_0' is `uniform' in fragment shader
      shader._setUniformLocInt("Texture_1", 1);
    
    // ------------  render loop -------------
    while (!glfwWindowShouldClose(window)) {
      // input
      processInput(window);
      // render
      glClearColor(0.2f,0.3f,0.3f,1.0f);
      glClear(GL_COLOR_BUFFER_BIT);
      // activate the texture unit first before binding texture
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, texture_id_0);
      glActiveTexture(GL_TEXTURE1);
      glBindTexture(GL_TEXTURE_2D, texture_id_1);
      
//      create transformations
      glm::mat4 transform = glm::mat4(1.0f); // init to identity matrix first
      transform = glm::translate(transform, glm::vec3(0.5f, -0.5f, 0.0f));
      transform = glm::rotate(transform, (float)glfwGetTime(), glm::vec3(0.0f, 0.0f, 1.0f));
      // ...........render container .......................
      shader.use();
        shader._setUniformLocMat4("Transform",transform);
      
      glBindVertexArray(VAO); // seeing as we only have a single VAO there's no need to bind it every time, but we'll do so to keep things a bit more organized
      glDrawElements(GL_TRIANGLES,6,GL_UNSIGNED_INT,0);
//        glDrawArrays(GL_TRIANGLES, 0, 3);
      // glBindVertexArray(0); // no need to unbind it every time
      
      glfwSwapBuffers(window);
      glfwPollEvents();
    }
    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
    
    
    glfwTerminate();
    return 0;
    }
    catch (const std::string& e) {
      std::cerr << PRG << e << std::endl;
    }
    catch (const std::system_error& e) {
      std::cerr << PRG << e.what() << " (" << e.code() << ")" << std::endl;
    }
    catch (...) {
      std::cerr << PRG << " caught an unknown exception" << std::endl;
    }
   
    glfwTerminate();
    return -1;
  
}
