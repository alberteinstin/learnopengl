//
//  OpenGL
//
//  Created by Albert Einstin on 11/18/19.
//  Copyright © 2019 Albert Einstin. All rights reserved.
//

#include <iostream>
#include <vector>
#include "glad/glad.h"
#include "GLFW/glfw3.h"


#include "Model.h"
#include "Camera.h"

#include "process.h"   // <- processInput,  setupWindow

float g_deltaTime = 0.f;// Time between current frame and last frame
float g_lastFrame = 0.f;// Time of last frame
float g_lastX = 0.f;
float g_lastY = 0.f;

Camera g_cam(glm::vec3(0.0f, 0.0f,  3.0f));  // init position in world
 
int main(int argc, const char * argv[])
{
  const char* const PRG = "main> ";
  using namespace std;

  const unsigned int SCR_WIDTH = 800, SCR_HEIGHT = 600;   //  view port size
  g_lastX = SCR_WIDTH/2.f;  g_lastY = SCR_HEIGHT/2.f;
  
//  glm::vec3 light_pos(1.2f, 1.0f, 1.0f); // light positon

  try {
    GLFWwindow* window = setupWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL");
    
    glEnable(GL_DEPTH_TEST); // configure global opengl state

      // ........... shader processing (compile+link+program on ctor)
      Shader shader(loc_home()+"shaders/model_loading.vs","model_loading.fs");
      cout << PRG << "shader(s) loaded, loading model: ...." << endl;
      Model backpack(loc_home()+"models/backpack/backpack.obj");
      cout << PRG << "....model loaded ---> good to go" << endl;
      
    // ------------  render loop --------------------------------------------------------------------------
    while (!glfwWindowShouldClose(window)) {
      
      float currentFrame = glfwGetTime();
      g_deltaTime = currentFrame - g_lastFrame;
      g_lastFrame = currentFrame;
      
      processInput(window, g_cam);                        // input from keyboard/mouse

      glClearColor(0.2f,0.3f,0.3f,1.0f);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear z-buffer
      
       // view/projection transformations
      // .............. render target cube (lit) ................
      auto project = glm::perspective(g_cam.FovInRad(), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
      auto view = g_cam.lookAt();
      shader.use();
      shader._setUniformLocMat4("View",view)._setUniformLocMat4("Project",project)._setUniformLocMat4("Model",glm::mat4(1.0f));

      backpack.draw(shader);      // <<<=== draw model
      
      // glBindVertexArray(0); // no need to unbind it every time
      glfwSwapBuffers(window);
      glfwPollEvents();
    }
  
    
    glfwTerminate();
    return 0;
  }
  catch (const std::string& e) {
    std::cerr << PRG << e << std::endl;
  }
  catch (const std::system_error& e) {
    std::cerr << PRG << e.what() << " (" << e.code() << ")" << std::endl;
  }
  catch (...) {
    std::cerr << PRG << " caught an unknown exception" << std::endl;
  }
    glfwTerminate();
    return -1;
}
