//
 //  OpenGL
//
//  Created by Albert Einstin on 11/18/19.
//  Copyright © 2019 Albert Einstin. All rights reserved.
//

#ifndef TEXTURE_H
#define TEXTURE_H

#include <glad/glad.h> // include glad to get all the required OpenGL headers
  
#include <string>
 
class Texture
{
 public:
  enum class eType {
    eDiffuse, eSpecular, eNormal, eHeight, eAmbient, eSize
  };
    static const char* TypeName[int(eType::eSize)]; // = {"texture_diffuse","texture_specular","texture_normal","texture_height","texture_ambient"};

public:

  // reads and builds the texture
  Texture(const std::string& PathName  = "", bool doFlip=true) : type_{eType::eDiffuse} {
    if (PathName != "")
      load_(PathName, doFlip);
  }
  Texture(const std::string& Name, const std::string& Dir, eType texType = eType::eDiffuse, bool doFlip=true): type_{texType}
    {
      if (Dir != "" &&  Name != "") {
     std::string path_name = Dir + "/" + Name;
      load_(path_name, doFlip);
      }
    }

  auto ID () const { return id_; }
  auto Type() const { return type_; }
  const auto Path () { return path_; } 
  
private:
   void load_(const std::string& texturePath, bool doFlip=true);

private:
  unsigned int id_;
  std::string path_;
  enum eType type_;

};
  
#endif
