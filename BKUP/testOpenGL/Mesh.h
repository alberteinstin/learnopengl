#ifndef MESH_H
#define MESH_H

#include <string>
#include <vector>
#include <glad/glad.h> // holds all OpenGL type declarations

#include "Shader.h" 
#include "Texture.h" 

namespace mesh
{
  struct Vertex
  {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 texCoords;
    glm::vec3 tangent;
    glm::vec3 bitangent;
  };
  
  
  class Mesh {
  private:
    // mesh Data
    std::vector<Vertex>       vertices_;
    std::vector<unsigned int> indices_;
    std::vector<Texture>      textures_;
    mutable unsigned int VAO_, VBO_, EBO_;
   
public: 
    Mesh(std::vector<Vertex>&& Vertices, std::vector<unsigned int>&& Indices, std::vector<Texture>&& Textures):
      vertices_{std::move(Vertices)},  indices_{std::move(Indices)},  textures_{std::move(Textures)}
      {
        setup_mesh_();
      }
    Mesh(const std::vector<Vertex>& Vertices, const std::vector<unsigned int>& Indices, const std::vector<Texture>& Textures):
     vertices_{Vertices},  indices_{Indices},  textures_{Textures}
     {
       setup_mesh_();
     }
    // render the mesh
    void draw(const Shader& shader) const;

  private: 
    void setup_mesh_() const;
    
  };
}
#endif
